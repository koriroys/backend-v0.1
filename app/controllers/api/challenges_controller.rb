class Api::ChallengesController < ApplicationController
  include Api::Follow
  include Api::Members
  include Api::Upload
  include Api::Relations
  include Api::Utils
  include Api::Hooks

  # Before action validation are applied on different methods in order to
  # prevent events that are not destined to be executed, this also prevents
  # repeating the code on each method
  before_action :authenticate_user!, except: [:index, :show, :index_projects, :index_needs, :followers, :get_id_from_name, :members_list]
  before_action :find_challenge, except: [:index, :create, :my_challenges, :can_create, :short_title_exist, :get_id_from_name]
  before_action :find_project, only: [:attach, :remove]
  before_action :find_project_relation, only: [:set_project_status]
  before_action :set_obj, except: [:index, :create, :show, :my_challenges, :can_create, :short_title_exist, :get_id_from_name]
  before_action :is_admin, only: [:update, :invite, :upload, :update_member, :remove_member,
                                  :create_external_hook, :update_external_hook, :delete_external_hook, :get_external_hooks]

  # Before action for the relations Api::Relations
  before_action :find_relation, only: [:follow, :clap]

  def index
    @pagy, @challenges = pagy(Challenge.where.not(status: 'draft').all)
    render json: @challenges
  end

  def my_challenges
    @challenges = Challenge.with_role(:owner, current_user)
    @challenges += Challenge.with_role(:admin, current_user)
    @challenges += Challenge.with_role(:member, current_user)
    @pagy, @challenges = pagy_array(@challenges.uniq)
    render json: @challenges
  end

  def can_create
    if current_user.has_role? :challenge_creator
      render json: {data: "Authorized"}, status: :ok
    else
      render json: {data: "Forbidden"}, status: :forbidden
    end
  end

  def create
    # TODO: ADD A ROLE CREATE CHALLENGE
    # IF YOU HAVE IT THEN YOU ARE GOOD TO GO
    # ELSE Booped !
    unless current_user.has_role? :challenge_creator
      render json: {data: "Forbidden"}, status: :forbidden and return
    end
    @challenge = Challenge.new(challenge_params)
    @challenge.users << current_user
    save_interests params[:challenge][:interests], @challenge
    save_skills params[:challenge][:skills], @challenge
    if @challenge.save
      current_user.add_role :owner, @challenge
      current_user.add_role :admin, @challenge
      current_user.add_role :member, @challenge
      render json: {id: @challenge.id, data: "Challenge created succefully"}, status: :created
    else
      render json: {data: "Something went wrong"}, status: :unprocessable_entity
    end
  end

  def show
		render json: @challenge, show_objects: true
  end

  def update
    unless current_user.has_role? :admin, @challenge
      render json: {data: "You are not an admin of this object"}, status: :forbidden and return
    end
    save_interests params[:challenge][:interests], @challenge
    save_skills params[:challenge][:skills], @challenge
    if @challenge.update_attributes(challenge_params)
      # @challenge.geocode('challenge')
      render json: {data: "Challenge has been updated"}, status: :ok
    else
      render json: {data: "Something went wrong"}, status: :unprocessable_entity
    end
  end

# This method add project to a challenge and changes its status
  def attach
    unless @challenge.projects.include?(@project)
      @challenge.projects << @project
      find_project_relation
      @relation.status = "pending"
      @relation.save!
      hook_new_project_challenge(@challenge.externalhooks, @project)
      render json: {data: "Project attached"}, status: :ok
      User.with_role(:owner, @challenge).map do |owner|
        NotificationEmailWorker.perform_async(owner.id, "A new project is trying to join your challenge, accept or reject it on your admin page", 'challenge/' + @challenge.id.to_s + '/edit#projects', "A project wants to join your challenge!")
      end
      NotificationEmailWorker.perform_async(current_user.id, "The project has been submitted. It will be examined by the challenge team to validate its commitment to the challenge. Please find the attachment for participation rules", 'challenge/' + @challenge.id.to_s, "Project Submitted!", Rails.root.join("public", "attachments/JOGL_Co-Immune_participation_rules.pdf"))

    else
      render json: {data: "Project is already attached"}, status: :ok
    end
  end
# The object relation is remove no status change
  def remove
    if @challenge.projects.include?(@project)
      @challenge.projects.delete(@project)
      render json: {data: "Project removed"}, status: :ok
    else
      render json: {data: "Project is not attached"}, status: :not_found
    end
  end

  def index_projects
    render json: @challenge, serializer: Api::ChallengeProjectSerializer
  end


  def index_needs
    render json: @challenge, serializer: Api::ChallengeNeedSerializer
  end

  def set_project_status
    if params[:status]
      @relation.status = params[:status]
      if @relation.save!
        render json: {data: "Status changed"}, status: :ok
        @challenge.add_user_from_projects
        @challenge.program.add_user_from_challenges
      else
        render json: {data: "Something went wrong"}, status: :unprocessable_entity
      end
    else
      render json: {data: "You need to provide a status in the json body"}
    end
  end
  #
  # def upload_document
  #   documents = params[:documents]
  #   if !documents.nil?
  #     documents.each do |document|
  #       @challenge.documents.attach(document)
  #     end
  #     if !@challenge.documents.attached?
  #       render json: {data: "Something went wrong"}, status: :unprocessable_entity and return
  #     end
  #     render json: {data: "documents uploaded"}, status: :ok
  #   end
  # end

  def destroy
    if @challenge.users.count == 1
      @challenge.destroy
      render json: {data: "Challenge destroyed"}, status: :ok
    else
      @challenge.update_attributes({status: "archived"})
      render json: {data: "Challenge archived"}, status: :ok
    end
  end

  private
    def find_challenge
      @challenge = Challenge.find(params[:id])
      if @challenge.nil?
        render json: {data: "Challenge was not found"}, status: :not_found
      end
    end

    def find_project_relation
      @relation = ChallengesProject.find_by(challenge_id: params[:id], project_id: params[:project_id])
      if @relation.nil?
        render json: {data: "Challenge Project Relation not found"}, status: :not_found
      end
    end

    def set_obj
      @obj = @challenge
    end

    def find_project
      @project = Project.find(params[:project_id])
      if @project.nil?
        render json: {data: "Project was not found"}, status: :not_found
      end
    end

    def challenge_params
      params.require(:challenge).permit(:title, :title_fr, :description, :description_fr, :short_description, :short_description_fr, :rules, :rules_fr, :faq, :faq_fr, :program_id, :country,
                                        :city, :address, :status, :launch_date, :end_date, :final_date, :skills, :interests, :short_title)
    end
end
