class Api::PostsController < ApplicationController
  include Api::Relations
  include Api::Upload
  include Api::Hooks

  # Before action validation are applied on different methods in order to
  # prevent events that are not destined to be executed, this also prevents
  # repeating the code on each method
  before_action :find_post, only: [:show, :update, :destroy, :comment, :upload_document, :follow, :clap]

  before_action :authenticate_user!, only: [:create, :update, :destroy, :comment, :upload, :follow, :clap]
  before_action :set_obj, except: [:index, :create, :show, :destroy, :comment]

  before_action :find_relation, only: [:follow, :clap]
  before_action :is_allowed, only: [:update, :destroy]

  # before_action :sanitize, only: [:create, :update]

  def show
    json_response(@post)
  end

  def create
    @post = Post.new(post_params)
    @post.user_id = current_user.id
    @feed = Feed.find(params[:post][:feed_id])
    @from = @feed.object_type.camelize.constantize.find(@feed[@feed.object_type+'_id'])
		@post.from_object = @from.class.name
		# change "community" wording to "group"
		post_from_object_name = @post.from_object != "Community" ? @post.from_object.downcase : "group"
		@post.from_id = @from.id
    if @from.class.name == 'User'
      @post.from_name = @from.first_name + " " + @from.last_name
			# @post.from_image = @from.logo_url
		else
      @post.from_name = @from.title
			# @post.from_image = @from.banner_url
		end
		if @from.class.name == 'Need'
      @post.from_need_project_id = @from.project.id
    end
    if @post.save and !@feed.nil? and !@from.nil?
      @feed.posts << @post
      if @from.class.name == 'Need'
        project_feed = Feed.find_by(project_id: @from.project.id)
        project_feed.posts << @post
        hooks = Externalhook.find_by(hookable_type: "Project", hookable_id: @from.project.id)
        unless hooks.nil?
          hook_new_post(hooks)
        end
      end
      if @from.class.name == 'Project' or @from.class.name == 'Challenge'
        hook_new_post(@from.externalhooks, @post)
      end
			@from.followers.map do |follower|
				# if someone posts on their profile
        if @post.user_id == @post.from_id && @post.from_object == "User"
					NotificationEmailWorker.perform_async(follower.user.id, current_user_name + " just posted on their profile", @post.from_object.downcase + '/' + @post.from_id.to_s + "#news", "New post on a " +  @post.from_object.downcase)
				else # else specify who posted on what object that you are following
					# don't trigger email if you posted in an object you are admin/owner
          if @post.user_id != follower.user.id
						NotificationEmailWorker.perform_async(follower.user.id, current_user_name + " posted on a " +  post_from_object_name + " you are following on JOGL: \'#{@post.from_name}\'", @post.from_object.downcase + '/' + @post.from_id.to_s + "#news", "New post on a " +  post_from_object_name)
					end
        end
      end

      if ((@post.from_object == 'Community' || @post.from_object == 'Project') && !@from.is_private) || @post.from_object == 'Challenge'  || @post.from_object == 'Program'
				get_object_members(@from).map do |receiver|
					# don't trigger email if you posted in an object you are admin/owner
          if @post.user_id != receiver.id
            NotificationEmailWorker.perform_async(receiver.id, current_user_name + " posted on your " +  post_from_object_name + ": " + @post.from_name, @post.from_object.downcase + '/' + @post.from_id.to_s + "#news", "New post on your " +  post_from_object_name)
          end
        end
      end
      # First let's render the JSON because the post went through
      render json: {id: @post.id, data: "Your post has been published"}, status: :created
      # Then we handle the different mentions "offline"
      save_mentions(params[:post][:mentions])
    else
      render json: {data: "Something went wrong"}, status: :unprocessable_entity
    end
  end

  def update
    if @post.update_attributes(post_params)
      render json: {data: "Post has been updated"}, status: :ok
    end
    # Now we handle the mentions
    save_mentions(params[:post][:mentions])
  end

  def destroy
    if @post.destroy
      render json: {data: "Your post has been deleted"}, status: :ok
    end
  end

  def comment
    case request.method_symbol
    when :post
      @comment = Comment.new(user_id: current_user.id,
                             post_id: @post.id,
                             content: params[:comment][:content])
			object = @post.from_object.constantize.find(@post.from_id)
			# change "community" wording to "group"
			post_from_object_name = @post.from_object != "Community" ? @post.from_object.downcase : "group"
			if @post.from_object == 'Need'
				the_url = "project/" + @post.from_need_project_id + "#needs"
			else
				the_url = @post.from_object.downcase + '/' + @post.from_id.to_s + "#news"
			end
      if @comment.save
				@post.comments << @comment
        if (@post.from_object != 'User') # if object is not user
					get_object_members(object).map do |receiver|
						# don't trigger email if you commented on an object you are admin/owner
            if @comment.user_id != receiver.id
              NotificationEmailWorker.perform_async(receiver.id, current_user_name + " commented on a post in your " + post_from_object_name + ": " + @post.from_name, the_url, "New Comment on your " + post_from_object_name)
            end
          end
				elsif (@post.from_object == 'User') # if object is user only
					# don't trigger email if you commented on your profile
					if @comment.user_id != object.id
						NotificationEmailWorker.perform_async(object.id, current_user_name + " commented on your profile", the_url, "New Comment on your profile!")
					end
				end

        @post.mentions.each do |mention|
					obj = get_object_mention(mention)
					# don't trigger email if you comment on a post in which you are mentioned
          if obj.class.name == 'User' && obj.id != current_user.id
            NotificationEmailWorker.perform_async(obj.id, current_user_name + " commented on a post in which you are mentioned", the_url, "New Comment!")
          end
        end
        # First let's render the JSON because the post went through
        render json: {data: "Your comment has been published"}, status: :created
        save_mentions(params[:comment][:mentions])
      end
    when :patch
      @comment = Comment.find(params[:comment_id])
      is_allowed @comment
      if @comment.nil?
        render json: {data: "Cannot find comment id"}
      end
      @comment.update_attributes(content: params[:comment][:content])
      render json: {data: "Your post has been updated"}, status: :ok
      save_mentions(params[:comment][:mentions])
    when :delete
      @comment = Comment.find(params[:comment_id])
      is_allowed @comment
      @comment.destroy
      render json: {data: "Comment deleted"}, status: :ok
    end
  end

  private

    def save_mentions(mentions)
      if !mentions.nil?
        mentions.map do |mention|
          # Create a mention in the table
          logger.debug(mention)
          @mention = Mention.new(post_id: @post.id,
                                 obj_type: mention[:obj_type],
                                 obj_id: mention[:obj_id],
                                 obj_match: mention[:obj_match])
          # We check that the mention is valid, this means that it has a unique set of post and object mentions
          # Basically we do not store double mentions as double but just once
          if @mention.valid?
            # The we do the whole logic for the mentions
            @object = get_object_mention(mention)
            if !@object.nil?
              @mention.save
              @post.mentions << @mention
              unless @object.feed.posts.exists?(@post.id)
                @object.feed.posts << @post
              end
            end

            send_mention_notification if @object.class.name == 'User'
          end
        end
      end
    end

    def find_post
      @post = Post.find(params[:id])
      if @post.nil?
        render json: {data: "Post not found"}, status: :not_found
      end
    end

    def set_obj
      @obj = @post
    end

    def get_object_mention(mention)
      @object = mention[:obj_type].humanize.constantize.find(mention[:obj_id])
      return @object
    end

    def post_params
      params.require(:post).permit(:user_id, :content, :media, :feed_id, :is_need)
    end

    def is_allowed(object=@post)
      unless object.user_id == current_user.id
        feed = Feed.find(@post.feed_id)
        parent = feed.object_type.camelize.constantize.find(feed[feed.object_type+"_id"])
        unless current_user.has_role? :admin, parent
          render json: {data: "Forbidden"}, status: :forbidden and return
        end
      end
    end

    def get_object_members(object)
      (User.with_role(:owner, object) + User.with_role(:admin, object)).uniq
    end

		def send_mention_notification
			if @post.from_object == 'Need'
				the_url = "project/" + @post.from_need_project_id + "#needs"
			else
				the_url = @post.from_object.downcase + '/' + @post.from_id.to_s + "#news"
			end
			# change "community" wording to "group"
			post_from_object_name = @post.from_object != "Community" ? @post.from_object.downcase : "group"
			if params[:comment].present?
				if @post.from_object == 'User' && @post.user_id != @post.from_id.to_s
				  NotificationEmailWorker.perform_async(@object.id, current_user_name + " mentioned you in a post's comment on their profile", the_url,  current_user_name + " mentioned you!")
				else
          NotificationEmailWorker.perform_async(@object.id, current_user_name + " mentioned you in a post's comment of the " +  post_from_object_name + ": " + @post.from_name, the_url,  current_user_name + " mentioned you!")
        end
			elsif params[:post].present?
				if @post.from_object == 'User' && @post.user_id != @post.from_id.to_s
          NotificationEmailWorker.perform_async(@object.id,  current_user_name + " mentioned you in a post on their profile", the_url,  current_user_name + " mentioned you!")
        else
          NotificationEmailWorker.perform_async(@object.id,  current_user_name + " mentioned you in a post of the " +  post_from_object_name + ": " + @post.from_name, the_url,  current_user_name + " mentioned you!")
        end
      end
    end

    def current_user_name
      [current_user.first_name, current_user.last_name].join(' ')
    end
end
