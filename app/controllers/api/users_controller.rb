class Api::UsersController < ApplicationController
  include Api::Follow
  include Api::Upload
  include Api::Relations
  include Api::Utils

  before_action :find_user, except: [:index, :create, :resend_confirmation, :nickname_exist]
  before_action :authenticate_user!, only: [:update, :destroy, :upload, :follow, :clap]
  before_action :is_self?, only: [:upload, :update, :destroy]
  before_action :set_obj, except: [:index, :create, :show, :destroy, :resend_confirmation, :nickname_exist]
  # Before action for the relations Api::Relations
  before_action :find_relation, only: [:follow, :clap]

  def index
    # @users = User.all
    @pagy, @users = pagy(User.all)
    render json: @users, each_serializer: Api::UserSerializer
  end

  def create
    @user = User.where(email: create_user_params[:email])
    if create_user_params[:password] != create_user_params[:password_confirmation]
      render json: {error: "Password confirmation does not match the password"}, status: :unprocessable_entity and return
    end
    if @user.blank?
      @user = User.new(create_user_params)
      @user.uid = @user.email
      @user.provider = "email"
      if @user.save
        # ConfirmEmailWorker.perform_async(@user.id, params[:redirect_url])

        render json: {msg: "Thank you for signing up, please confirm your email address to continue"}, status: :created
      else
        json_response(@user.errors)
      end
    else
      render json: {error: "User already exists"}, status: :unprocessable_entity
    end
  end

  def show
    render json: @user, each_serializer: Api::UserSerializer
  end

  def update
    user_interests = params[:user][:interests]
    if !user_interests.nil?
      @user.interests.delete_all
      user_interests.each do |interest_id|
        @interest = Interest.find(interest_id)
        if @interest
          @user.interests <<  @interest
        else
          render json: {data: "Could not find interest"}, status: :not_found and return
        end
      end
    end
    save_skills params[:user][:skills], @user
    save_ressources params[:user][:ressources], @user
    # user_skills = params[:user][:skills]
    # if !user_skills.nil?
    #   @user.skills.delete_all
    #   user_skills.each do |skill|
    #     @skill = Skill.find_by(skill_name: skill)
    #     if @skill.nil?
    #       @user.skills << Skill.new(skill_name: skill)
    #     else
    #       @user.skills << @skill
    #     end
    #   end
    # end
    nickname = params[:user][:nickname]
    if !nickname.nil?
      @other_user = User.find_by(nickname: nickname)
      if !@other_user.nil? and @other_user.id != @user.id
        render json: {data: "Nickname is already taken"}, status: :unprocessable_entity and return
      end
    end
    if @user.update_attributes(user_params)
      # @user.geocode('user')
      render json: {data: "User updated"}, status: :ok
    else
      render json: {data: "Something went wrong :("}, status: :unprocessable_entity
    end
  end

  def destroy
    current_user.status = 1
    if current_user.save
      current_user.projects.map do |project|
        project.users.delete(current_user)
        current_user.remove_role :member, project
        current_user.remove_role :admin, project
        current_user.remove_role :owner, project
      end
      current_user.challenges.map do |challenge|
        challenge.users.delete(current_user)
        current_user.remove_role :member, challenge
        current_user.remove_role :admin, challenge
        current_user.remove_role :owner, challenge
      end
      current_user.communities.map do |community|
        community.users.delete(current_user)
        current_user.remove_role :member, community
        current_user.remove_role :admin, community
        current_user.remove_role :owner, community
      end
      current_user.needs.map do |need|
        need.users.delete(current_user)
        current_user.remove_role :member, need
        current_user.remove_role :admin, need
        current_user.remove_role :owner, need
      end
      Relation.where(user_id: current_user.id).destroy_all
      json_response(current_user)
    end
  end

  def confirm_email
    @user = User.where(confirm_token: params[:token]).first
    if @user
      @user.validate_email
      if @user.save
	      if ENV["RAILS_ENV"] == "development"
	          redirect_to "http://localhost:3000/newjogler"
       	end
      end
    else
      render json: {data: "Sorry. User does not exist"}, status: :not_found
    end
  end

  def projects
    users_projects = current_user.projects
    render json: users_projects, each_serializer: Api::ProjectSerializer
  end

  def user_object
    klass = params[:object_type].singularize.camelize.constantize
    serializer = "Api::" + params[:object_type].singularize.camelize + "Serializer"
    @results = klass.with_role(:owner, @user)
    @results += klass.with_role(:admin, @user)
    @results += klass.with_role(:member, @user)
    render json: @results.uniq, each_serializer: serializer.constantize
	end

	def send_private_email
		unless params[:object].nil? or params[:content].nil?
			PrivateEmailWorker.perform_async(current_user.id, @user.id, params[:object], params[:content])
			render json: {data: "Message sent"}, status: :ok
		else
			render json: {data: "Something went wrong :("}, status: :unprocessable_entity
		end
	end

  def resend_confirmation
    unless user_params[:email]
      return render json: {
        success: false,
        errors: ['You must provide an email address.']
      }, status: 400
    end

    @user = User.find_by(email: user_params[:email])

    errors = nil

    if @user
      if @user.confirmed_at.present?
        errors = ["User already confirmed"]
      else
        @user.resend_confirmation_instructions
      end
    else
      errors = ["Unable to find user with email '#{user_params[:email]}'."]
    end

    if errors
      render json: {
        success: false,
        errors: errors
      }, status: 400
    else
      render json: {
        status: 'success',
        data:   @user.as_json
      }
    end
  end

  private
    def find_user
      @user = User.find(params[:id])
      if @user.nil?
        render json: {data: "User not found"}, status: :not_found
      end
    end

    def set_obj
      @obj = @user
    end

    def is_self?
      if current_user.id != params[:id].to_i
        render json: {data: "Only I can update my mind!"}, status: :forbidden and return
      end
    end

    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation, :first_name, :last_name, :nickname,
        :age, :country, :city, :address, :phone_number, :bio, :short_bio, :can_contact, :status, :other_user_id,
        :affiliation, :category, :mail_newsletter, :mail_weekly, :logo_url, :ressources)
    end

    def create_user_params
      params.require(:user).permit(:email, :password, :password_confirmation, :first_name, :last_name,
                                    :nickname, :mail_newsletter, :mail_weekly, :logo_url)
    end
end
