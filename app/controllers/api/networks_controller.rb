class Api::NetworksController < ApplicationController
  # Before action validation are applied on different methods in order to
  # prevent events that are not destined to be executed, this also prevents
  # repeating the code on each method
  before_action :find_network, only: [:show, :destroy]
  # before_action :authenticate_user!, only: [:create, :destroy]

  # before_action :sanitize, only: [:create, :update]

  def index
    networks = Network.all.map do |network|
      {
        id: network.id,
        created_at: network.created_at,
        updated_at: network.updated_at,
      }
    end
    json_response(networks)
  end

  def show
    json_response(@network)
  end

  def create
    NetworkMakerWorker.perform_async
    render json: {data: "The network is being created... please check back on the API"}, status: :ok
  end

  def destroy
    if @network.destroy
      render json: {data: "The network has been deleted"}, status: :ok
    else
      render json: {data: "Something went wrong"}, status: :unprocessable_entity
    end
  end

  private

    def find_network
      @network = Network.find(params[:id])
      if @network.nil?
        render json: {data: "Post not found"}, status: :not_found
      end
    end
end
