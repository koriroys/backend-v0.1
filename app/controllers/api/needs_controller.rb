class Api::NeedsController < ApplicationController
  include Api::Relations
  include Api::Members
  include Api::Utils
  include Api::Upload
  include Api::Hooks



  # Before action validation are applied on different methods in order to
  # prevent events that are not destined to be executed, this also prevents
  # repeating the code on each method
  before_action :find_need, except: [:create, :index]
  before_action :find_project, only: [:create]

  before_action :authenticate_user!, only: [:create, :update, :destroy, :comment,
                                            :upload_document, :follow, :clap, :join, :leave]
  before_action :set_obj, except: [:index, :create, :show, :destroy, :comment]


  before_action :find_relation, only: [:follow, :clap]

  before_action :can_create, only: [:create, :upload_document]
  before_action :can_update, only: [:update, :destroy]
  # before_action :sanitize, only: [:create, :update]

	def index
		# take order param in the url, and order array depending on its value
		if params[:order] == "desc"
			param = 'id DESC'
		else
			param = 'id ASC'
		end
    @pagy, @needs = pagy(Need.order(param).all)
    render json: @needs, each_serializer: Api::NeedSerializer, status: :ok
  end

  def show
    json_response(@need)
  end

  def create
    @need = Need.new(need_params)
    @need.user_id = current_user.id
    @need.status = :active
    if @need.save!
      save_skills params[:need][:skills], @need
      save_ressources params[:need][:ressources], @need
      @project.needs << @need
      current_user.add_role :owner, @need
      current_user.add_role :member, @need
      # First let's render the JSON because the need went through
      render json: {id: @need.id, data: "Your need has been published"}, status: :created
      hook_new_need(@project.externalhooks, @need)

    else
      render json: {data: "Something went wrong"}, status: :unprocessable_entity
    end
  end

  def update
    if @need.update_attributes(need_params)
      save_skills params[:need][:skills], @need
      save_ressources params[:need][:ressources], @need
      render json: {data: "Need has been updated"}, status: :ok
    end
  end

  def destroy
    if @need.destroy
      render json: {data: "Your need has been deleted"}, status: :ok
    end
  end
  #
  # def upload_document
  #   documents = params[:documents]
  #   if !documents.nil?
  #     documents.each do |document|
  #       @need.documents.attach(document)
  #     end
  #     if !@need.documents.attached?
  #       render json: {data: "Something went wrong"}, status: :unprocessable_entity and return
  #     end
  #     render json: {data: "documents uploaded"}, status: :ok
  #   end
  # end

  private

    def find_need
      @need = Need.find(params[:id])
      if @need.nil?
        render json: {data: "Need not found"}, status: :not_found
      end
    end

    def find_project
      @project = Project.find(params[:need][:project_id])
      if @project.nil?
        render json: {data: "Project not found"}, status: :not_found
      end
    end

    def can_create
      if @project.nil?
        @project = Project.find(@need.project_id)
      end
      unless current_user.has_role? :member, @project
        render json: {data: "You must be a member of this project to create a need"}, status: :forbidden
      end
    end

    def can_update
      unless current_user.has_role? :owner, @need or current_user.has_role? :admin, @project
        render json: {data: "Only the creator of the need or an admin can update it"}, status: :forbidden
      end
    end

    def set_obj
      @obj = @need
    end

    def need_params
      params.require(:need).permit(:user_id, :project_id, :title, :content, :status, :is_urgent,
                                    :address, :city, :country, :ressources)
    end

end
