class Api::FeedsController < ApplicationController
  include Api::Utils

  before_action :find_feed, only: [:show, :remove_post]
  before_action :authenticate_user!, only: [:index, :remove_post]
  before_action :set_obj, only: [:remove_post]
  before_action :is_admin, only: [:remove_post]
  before_action :make_user_feed, only: [:index]
  before_action :find_post, only: [:remove_post]

  # before_action :sanitize, only: [:create, :update]

  def index
    @pagy, @myfeed = pagy(@posts)
    render json: @myfeed, each_serializer: Api::PostSerializer, status: :ok
  end

  def show
    @pagy, @somefeed = pagy(@feed.posts.order('created_at DESC'))
    render json: @somefeed, each_serializer: Api::PostSerializer, status: :ok
  end

  def remove_post
    @feed.posts.delete(@post)
    render json: {data: "Post is deleted"}, status: :ok
  end

  private

    def set_obj
      @obj = @feed.object_type.camelize.constantize.find(@feed[@feed.object_type+'_id'])
    end

    def find_feed
      @feed = Feed.find(params[:id])
      if @feed.nil?
        render json: {data: "Feed not found"}, status: :not_found
      end
    end

    def find_post
      @post = Post.find(params[:post_id])
      if @post.nil?
        render json: {data: "Post not found"}, status: :not_found
      end
    end

    def make_user_feed
      posts_ids = []
      posts_ids += current_user.feed.posts.ids
			current_user.following.map do |object|
				if object.resource_type.constantize.exists? object.resource_id
					@object = object.resource_type.constantize.find(object.resource_id)
          posts_ids += @object.feed.posts.ids
				end
      end
      @posts = Post.where(id: posts_ids).order(created_at: :desc)
    end
end
