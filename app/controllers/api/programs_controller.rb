class Api::ProgramsController < ApplicationController
  include Api::Follow
  include Api::Members
  include Api::Upload
  include Api::Relations
  include Api::Utils

  # Before action validation are applied on different methods in order to
  # prevent events that are not destined to be executed, this also prevents
  # repeating the code on each method
  before_action :find_program, except: [:index, :create, :my_programs, :get_id_from_name, :short_title_exist]
  before_action :authenticate_user!, only: [:create, :update, :destroy, :invite, :join, :follow, :clap,
                                            :upload, :update_member, :my_programs]
  before_action :find_challenge, only: [:attach, :remove]

  before_action :set_obj, except: [:index, :create, :show, :destroy, :short_title_exist, :get_id_from_name]
  before_action :is_admin, only: [:update, :invite, :upload, :update_member, :remove_member]
  # Before action for the relations Api::Relations
  before_action :find_relation, only: [:follow, :clap]
  before_action :can_create, only: [:create]

  def index
    @pagy, @programs = pagy(Program.where.not(status: 'draft').all)
		render json: @programs
  end

  def my_programs
    @programs = Program.with_role(:owner, current_user)
    @programs += Program.with_role(:admin, current_user)
    @programs += Program.with_role(:member, current_user)
    @pagy, @programs = pagy_array(@programs.uniq)
		render json: @programs
  end

  def create
    @program = Program.new(program_params)
    @program.status = 'draft'
    @program.users << current_user
    if @program.save
      current_user.add_role :owner, @program
      current_user.add_role :admin, @program
      current_user.add_role :member, @program
      render json: {id: @program.id, data: "Success"}, status: :created
    end
  end

  def show
		render json: @program, show_objects: true
  end

  def getid
    @program = Program.where(short_title: params[:nickname]).first
    render json: {id: @program.id, data: "Success"}, status: :ok
  end

  def update
    if @program.update_attributes(program_params)
      if @program.save!
        render json: @program, status: :ok
      end
    else
      render json: {data: "Something went wrong :("}, status: :unprocessable_entity
    end
  end

  def destroy
    # why count the users in a program?
    # this would be more logical @program.users.delete_all followed by status modification
    # Leo: Because if the program has only one user we consider it a "test" aka you can ACTUALLY deleted it
    # Else, it stays as archived
    if @program.users.count == 1
      @program.destroy
      render json: {data: "Program destroyed"}, status: :ok
    else
      @program.update_attributes({status: "archived"})
      render json: {data: "Program archived"}, status: :ok
    end
  end

  # This method add challenge to a program and changes its status
    def attach
      unless @program.challenges.include?(@challenge)
        @program.challenges << @challenge
        render json: {data: "Challenge attached"}, status: :ok
      else
        render json: {data: "Challenge is already attached"}, status: :ok
      end
    end

  # The object relation is remove no status change
    def remove
      if @program.challenges.include?(@challenge)
        @program.challenges.delete(@challenge)
        render json: {data: "Challenge removed"}, status: :ok
      else
        render json: {data: "Challenge is not attached"}, status: :not_found
      end
    end

    def index_challenges
      render json: @program, serializer: Api::ProgramChallengeSerializer
    end

    def index_projects
      render json: @program, serializer: Api::ProgramProjectSerializer
    end

    def index_needs
      render json: @program, serializer: Api::ProgramNeedSerializer
    end

  private
    def find_program
      @program = Program.find(params[:id])
      if @program.nil?
        render json: {data: "Program not found"}, status: :not_found
      end
    end

    def find_challenge
      @challenge = Challenge.find(params[:challenge_id])
      if @challenge.nil?
        render json: {data: "Challenge was not found"}, status: :not_found
      end
    end

    def set_obj
      @obj = @program
    end

    def can_create
      unless current_user.has_role? :program_creator
        render json: {data: "You do not have the right to create a program"}, status: :forbidden
      end
    end

    def program_params
      params.require(:program).permit(:title, :title_fr, :short_title, :short_title_fr, :short_description, :short_description_fr, :description, :description_fr, :status, :launch_date, :end_date, :faq, :faq_fr, :enablers, :enablers_fr, :ressources)
    end
end
