class ApplicationController < ActionController::API
  include DeviseTokenAuth::Concerns::SetUserByToken
  include ActionController::MimeResponds
  include Response
  include ExceptionHandler
  include Pagy::Backend
  respond_to :json

  before_action :configure_permitted_parameters, if: :devise_controller?
  after_action { pagy_headers_merge(@pagy) if @pagy }
  after_action :save_user_ip
  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_in)
  end

  def save_user_ip
    unless current_user.nil?
      if current_user.ip != request.remote_ip
        current_user.ip = request.remote_ip
        current_user.save
        # current_user.geocode('user')
      end
    end
  end

# This block is used for LogRage to add information into Payload. See lograge.rb to then send this info into the logstack
  # def append_info_to_payload(payload)
  #   super
  #   payload[:request_ip] = request.ip
  # end

end
