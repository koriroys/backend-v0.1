module Api::Customfields

  def create_custom_field
    field = @obj.customfields.create(validate_fields)
    unless field.nil?
      render json: {data: "Fields created"}, status: :created
    else
      render json: {data: "an error as occured"}, status: :unprocessable_entity
    end
  end

  def update_custom_field
    @field = Customfield.find(params[:field_id])
    unless @field.nil?
      if @field.update_attributes(validate_fields)
        render json: {data: "Fields updated"}, status: :ok
      else
        render json: {data: "an error as occured"}, status: :unprocessable_entity
      end
    else
      render json: {data: "Field ot found"}, status: :not_found
    end
  end

  def delete_custom_field
    @field = Customfield.find(params[:field_id])
    unless @field.nil?
      # Delete the data associated first
      @field.customdatas.all.map do |data|
        data.delete
      end
      #Then delete the field itself
      if @field.delete
        render json: {data: "Fields deleted"}, status: :ok
      else
        render json: {data: "an error as occured"}, status: :unprocessable_entity
      end
    else
      render json: {data: "Missing field id to update"}, status: :unprocessable_entity
    end
  end

  def get_custom_fields
    render json: @obj.customfields, each_serializer: Api::CustomfieldSerializer
  end

  def create_custom_data
    @customfield = Customfield.find(params[:field_id])
    unless @customfield.nil?
      @customfield.customdatas.create(user_id: current_user.id, value: validate_data[:value])
      render json: {data: "Data entered"}, status: :created
    else
      render json: {data: "Customfield " + params[:field_id] + " not found"}, status: :not_found
    end
  end

  def update_custom_data
    @customfield = Customfield.find(params[:field_id])
    unless @customfield.nil?
      @customdata = @customfield.customdatas.find_by(user_id: current_user.id)
      unless @customdata.nil?
        if @customdata.update_attributes(validate_data)
          render json: {data: "Data updated"}, status: :ok
        else
          render json: {data: "an error as occured"}, status: :unprocessable_entity
        end
      else
        render json: {data: "Customdata not found"}, status: :not_found
      end
    else
      render json: {data: "Customfield " + params[:field_id] + " not found"}, status: :not_found
    end
  end

  # def delete_custom_data
  #
  # end

  def get_custom_data
    @customfield  = Customfield.find(params[:field_id])
    render json: @customfield.customdatas, each_serializer: Api::CustomdataSerializer
  end

  def get_my_custom_datas
    answers = []
    unless current_user.nil?
      @obj.customfields.map do |customfield|
        answer = customfield.customdatas.find_by(user_id: current_user.id)
        unless answer.nil?
          answers << answer
        end
      end
      render json: answers, each_serializer: Api::CustomdataSerializer
    else
      render json: {data: "Forbidden"}, status: :forbidden
    end
  end

  def validate_fields
    params.require(:fields).permit(:description, :name, :field_type, :optional)
  end

  def validate_data
    params.require(:data).permit(:value)
  end


  #
  # def is_admin
  #   unless current_user.has_role? :admin, @obj
  #     render json: {data: "Forbidden"}, status: :forbidden
  #   end
  # end
  #
  # def save_skills(skills, obj)
  #   unless skills.nil?
  #     obj.skills.delete_all
  #     skills.each do |skill|
  #       @skill = Skill.find_by(skill_name: skill)
  #       if @skill.nil?
  #          @skill = Skill.new(skill_name: skill)
  #       end
  #       obj.skills << @skill
  #     end
  #   end
  # end
  #
  # def save_interests(interests, obj)
  #   unless interests.nil?
  #     obj.interests.delete_all
  #     interests.each do |interest|
  #       obj.interests << Interest.find(interest)
  #     end
  #   end
  # end
  #
  # def nickname_exist
  #   if User.where(nickname: params[:nickname]).count > 0
  #     render json: {data: "Nickname already exists"}, status: :forbidden
  #   else
  #     render json: {data: "Nickname is available"}, status: :ok
  #   end
  # end
  #
  # def short_title_exist
  #   klass = controller_name.classify.constantize
  #   # klass = params[:object_type].capitalize.singularize.constantize
  #   if klass.where(short_title: params[:short_title]).count > 0
  #     render json: {data: "short_title already exists"}, status: :forbidden
  #   else
  #     render json: {data: "short_title is available"}, status: :ok
  #   end
  # end
  #
  # def get_id_from_name
  #   klass = controller_name.classify.constantize
  #   @obj = klass.where(short_title: params[:short_title]).first
  #   unless @obj.nil?
  #     render json: {id: @obj.id, data: "Success"}, status: :ok
  #   else
  #     render json: {data: "short_title does not exists"}, status: :not_found
  #   end
  # end

end
