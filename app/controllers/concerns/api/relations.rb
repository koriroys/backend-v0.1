module Api::Relations
  extend ActiveSupport::Concern

  def follow
    case request.method_symbol
      when :put
        @relation.follows = true
        if @relation.resource_type == 'User'
          receiver = @relation.resource_type.constantize.find(@relation.resource_id)
          NotificationEmailWorker.perform_async(receiver.id, current_user_name + ' is now following you', @relation.resource_type.downcase + '/' + current_user.id.to_s, current_user_name + ' is now following you')
        end
        save_relation("Followed")
      when :get
        if @relation.follows
          render json: {follows: true}, status: :ok
        else
          render json: {follows: false}, status: :ok
        end
      when :delete
        @relation.follows = false
        save_relation("Unfollowed")
    end
  end

	def clap
    case request.method_symbol
      when :put
        @relation.has_clapped = true
        save_relation("Clapped")
      when :get
        if @relation.has_clapped
          render json: {has_clapped: true}, status: :ok
        else
          render json: {has_clapped: false}, status: :ok
        end
      when :delete
        @relation.has_clapped = false
        save_relation("Unclapped")
    end
  end

  def clappers
    render json: @obj, serializer: Api::ClappersSerializer
  end

  private
    def save_relation(action)
      if @relation.save
        render json: {data: "#{action} successfully"}, status: :ok
      else
        render json: {data: "Something went wrong"}, status: :unprocessable_entity
      end
    end

    def relation_on_empty
     if @relation.nil?
       @relation = Relation.new(user_id: current_user.id, resource_type: @obj.class.name, resource_id: @obj.id)
     end
		end

    def relation_exists?
      if @relation.nil?
        render json: {data: "Relation does not exist"}, status: :not_found
      end
    end

		def find_relation
      @relation = Relation.find_by(user_id: current_user.id, resource_type: @obj.class.name, resource_id: @obj.id)
      case request.method_symbol
        when :put
          relation_on_empty
        when :get
          relation_exists?
        when :delete
          relation_exists?
      end
    end

    def relation_params
      params.permit(:resource_type, :resource_id)
    end

    def current_user_name
      [current_user.first_name, current_user.last_name].join(' ')
    end
end
