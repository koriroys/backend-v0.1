module Api::Utils
  def is_admin
    unless current_user.nil?
      if @obj.nil?
        render json: {data: "Obj is not set"}, status: :not_found
      else
        if @obj.class.name == "Need"
          unless current_user.has_role? :admin, @obj.project or current_user.has_role? :admin, @obj
            render json: {data: "Forbidden"}, status: :forbidden
          end
        else
          unless current_user.has_role? :admin, @obj
            render json: {data: "Forbidden"}, status: :forbidden
          end
        end
      end
    else
      render json: {data: "unauthorized"}, status: :unauthorized
    end
  end

  def is_member
    unless current_user.nil?
      unless current_user.has_role? :member, @obj
        render json: {data: "Forbidden"}, status: :forbidden
      end
    else
      render json: {data: "Unauthorized"}, status: :unauthorized
    end
  end

  def save_skills(skills, obj)
    unless skills.nil?
      obj.skills.delete_all
      skills.each do |skill|
        @skill = Skill.find_by(skill_name: skill)
        if @skill.nil?
           @skill = Skill.new(skill_name: skill)
        end
        obj.skills << @skill
      end
    end
  end

  def save_ressources(ressources, obj)
    unless ressources.nil?
      obj.ressources.delete_all
      ressources.each do |ressource|
        @ressource = Ressource.find_by(ressource_name: ressource)
        if @ressource.nil?
           @ressource = Ressource.new(ressource_name: ressource)
        end
        obj.ressources << @ressource
      end
    end
  end

  def save_interests(interests, obj)
    unless interests.nil?
      obj.interests.delete_all
      interests.each do |interest|
        obj.interests << Interest.find(interest)
      end
    end
  end

  def nickname_exist
    if User.where(nickname: params[:nickname]).count > 0
      render json: {data: "Nickname already exists"}, status: :forbidden
    else
      render json: {data: "Nickname is available"}, status: :ok
    end
  end

  def short_title_exist
    klass = controller_name.classify.constantize
    # klass = params[:object_type].capitalize.singularize.constantize
    if klass.where(short_title: params[:short_title]).count > 0
      render json: {data: "short_title already exists"}, status: :forbidden
    else
      render json: {data: "short_title is available"}, status: :ok
    end
  end

  def get_id_from_name
    klass = controller_name.classify.constantize
    @obj = klass.where(short_title: params[:short_title]).first
    unless @obj.nil?
      render json: {id: @obj.id, data: "Success"}, status: :ok
    else
      render json: {data: "short_title does not exists"}, status: :not_found
    end
  end

end
