module Api::Upload
  include Rails.application.routes.url_helpers

  def upload_avatar
    if !params[:avatar].nil?
      @obj.avatar.attach(params[:avatar])
      if @obj.avatar.attached?
        variant = @obj.avatar.variant(resize: "200x200^")
        logo_url = rails_representation_url(variant)
        render json: {data: "Avatar uploaded", url: logo_url}, status: :ok
        @obj.save!
      else
        render json: {data: "Something went wrong"}, status: :unprocessable_entity
      end
    end
  end

  def upload_banner
    if !params[:banner].nil?
      @obj.banner.attach(params[:banner])
      if @obj.banner.attached?
        render json: {data: "Banner uploaded", url: Rails.application.routes.url_helpers.rails_blob_url(@obj.banner)}, status: :ok
        @obj.save!
      else
        render json: {data: "Something went wrong"}, status: :unprocessable_entity
      end
    end
  end


  def upload_document
    documents = params[:documents]
    if !documents.nil?
      documents.each do |document|
        @obj.documents.attach(document)
      end
      if !@obj.documents.attached?
        render json: {data: "Something went wrong"}, status: :unprocessable_entity and return
      end
      render json: {data: "documents uploaded"}, status: :ok
    else
      render json: {data: "No documents"}, status: :unprocessable_entity
    end
  end

  def remove_document
    doc_id = params[:document_id]
    unless doc_id.nil?
      if @obj.documents.attached?
        @obj.documents.each do |document|
          if document.blob.id == doc_id.to_i
            document.purge
            render json: {data: "Document removed"}, status: :ok and return
          end
        end
        render json: {data: "Cannot find document!"}, status: :not_found and return
      end
      render json: {data: "Object has no document!"}, status: :not_found
    end
  end

  def remove_avatar
    if @obj.avatar.attached?
    	@obj.avatar.purge
      render json: {data: "Avatar removed"}, status: :ok
		else
			render json: {data: "No avatar attached"}, status: :not_found
    end
  end

  def remove_banner
    if @obj.banner.attached?
			@obj.banner.purge
      render json: {data: "Banner removed"}, status: :ok
		else
			render json: {data: "No banner attached"}, status: :not_found
    end
  end



end
