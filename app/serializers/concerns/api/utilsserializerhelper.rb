module Api::Utilsserializerhelper

  # Return a good format for logo of the object
  def logo_url(obj=nil)
    if obj.nil?
      obj = object
    end
    if !obj.avatar.attachment.nil?
      if obj.avatar.variable?
        variant = obj.avatar.variant(resize: "200x200^")
        Rails.application.routes.url_helpers.rails_representation_url(variant)
      else
        Rails.application.routes.url_helpers.rails_blob_url(obj.avatar)
      end
    else
      obj.logo_url
    end
	end

  # Return a good format for logo of the object
  def logo_url_sm(obj=nil)
    if obj.nil?
      obj = object
    end
    if !obj.avatar.attachment.nil?
      if obj.avatar.variable?
        variant = obj.avatar.variant(resize: "80x80^")
        Rails.application.routes.url_helpers.rails_representation_url(variant)
      else
        Rails.application.routes.url_helpers.rails_blob_url(obj.avatar)
      end
    else
      obj.logo_url
    end
  end


  # Return a good format for banner of the object
  def banner_url(obj=nil)
    if obj.nil?
      obj = object
    end
    if !obj.banner.attachment.nil?
      if obj.banner.variable?
        variant = obj.banner.variant(resize: "400x400^")
        Rails.application.routes.url_helpers.rails_representation_url(variant)
      else
        Rails.application.routes.url_helpers.rails_blob_url(obj.banner)
      end
    else
      ""
    end
  end

  # Return a good format for banner of the object
  def banner_url_sm(obj=nil)
    if obj.nil?
      obj = object
    end
    if !obj.banner.attachment.nil?
      if obj.banner.variable?
        variant = obj.banner.variant(resize: "100x100^")
        Rails.application.routes.url_helpers.rails_representation_url(variant)
      else
        Rails.application.routes.url_helpers.rails_blob_url(obj.banner)
      end
    else
      ""
    end
  end

	def documents
    if object.documents.attached?
      object.documents.attachments.map do |document|
        url = Rails.application.routes.url_helpers.rails_blob_url(document)
        if document.image?
          variant = document.variant(resize: "400x400^")
          url_variant = Rails.application.routes.url_helpers.rails_representation_url(variant)
        else
          url_variant = url
        end
        {
          id: document.blob.id,
          content_type: document.blob.content_type,
          filename: document.blob.filename,
          url_variant: url_variant,
          url: url
        }
      end
    else
      []
    end
	end

	def documents_feed
    unless object.feed.nil?
			attachments = get_feed_attachments(object)
			attachments.flatten
		end
  end


	def get_feed_attachments(obj)
		obj.feed.posts.map do |post|
			if post.documents.attached?
				post.documents.attachments.map do |document|
          url = Rails.application.routes.url_helpers.rails_blob_url(document)
          if document.image?
            variant = document.variant(resize: "400x400^")
            url_variant = Rails.application.routes.url_helpers.rails_representation_url(variant)
          else
            url_variant = url
          end
					{
						id: document.blob.id,
						content_type: document.blob.content_type,
						filename: document.blob.filename,
            url_variant: url_variant,
            url: url
					}
				end
			end
		end
	end


  def geoloc(obj = nil)
    if obj.nil?
      obj = object
    end
    {
      lat: obj.latitude,
      lng: obj.longitude
    }
  end

  def interests(obj=nil)
    if obj.nil?
      obj = object
    end
    obj.interests.map do |interest|
      interest.id
    end
  end

  def skills(obj=nil)
    if obj.nil?
      obj = object
    end
    obj.skills.map do |skill|
      skill.skill_name
    end
  end

  def ressources(obj=nil)
    if obj.nil?
      obj = object
    end
    obj.ressources.map do |ressource|
      ressource.ressource_name
    end
  end

  def feed_id
    object.feed.id
  end

  def needs_count(obj=nil)
    if obj.nil?
      obj = object
    end
    obj.needs.count
  end

end
