module Api::Usersserializerhelper

  include Rails.application.routes.url_helpers

  def users(obj = nil)
    if obj.nil?
      obj = object
    end
    obj.users.map do |user|
      logo_url = get_logo_url(user)
      {
        id: user.id,
        first_name: user.first_name,
        last_name: user.last_name,
        owner: user.has_role?(:owner, obj),
        admin: user.has_role?(:admin, obj),
        member: user.has_role?(:member, obj),
				logo_url: logo_url,
        logo_url_sm: user.logo_url_sm,
				short_bio: user.short_bio,
        geoloc: {
          lat: user.latitude,
          lng: user.longitude
        }
      }
    end
  end

  def members_count(obj=nil)
    if obj.nil?
      obj = object
    end
    User.with_role(:member, obj).count
  end

  def creator(obj=nil)
    if obj.nil?
      obj = object
    end
    if obj.class.name == "Post" or obj.class.name == "Comment" or obj.class.name == "Need"
      if User.exists? obj.user_id
        user = User.find(obj.user_id)
        logo_url = get_logo_url(user, variant_size="80x80^")
      else
        user = {id: -1, first_name: "User", last_name: "Deleted"}
        logo_url = "/static/media/default-user-1.2e46be09.png"
			end
    else
      if User.exists? obj.creator_id
        user = User.find(obj.creator_id)
        logo_url = get_logo_url(user)
      else
        user = {id: -1, first_name: "User", last_name: "Deleted"}
        logo_url = "/static/media/default-user-1.2e46be09.png"
			end
    end
    {
      id: user.id,
      first_name: user.first_name,
      last_name: user.last_name,
      logo_url: logo_url
    }
  end

  def get_logo_url(user, variant_size="200x200^")
    if user.avatar.attached?
      if user.avatar.image?
        variant = user.avatar.variant(resize: variant_size)
        logo_url = Rails.application.routes.url_helpers.rails_representation_url(variant)
      else
        logo_url = "/static/media/default-user-1.2e46be09.png"
      end
      # logo_url= Rails.application.routes.url_helpers.rails_blob_url(user.avatar)
    else
      logo_url = user.logo_url
    end
  end

end
