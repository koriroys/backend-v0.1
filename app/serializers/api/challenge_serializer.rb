class Api::ChallengeSerializer < ActiveModel::Serializer
  include Api::Relationsserializerhelper
  include Api::Rolesserializerhelper
  include Api::Usersserializerhelper
  include Api::Utilsserializerhelper

  attributes :id,
             :title,
             :title_fr,
             :banner_url,
             :banner_url_sm,
             :short_title,
             :short_description,
             :short_description_fr,
             :rules,
             :rules_fr,
             :faq,
             :faq_fr,
             :status,
             :skills,
             :interests,
             :documents,
             :geoloc,
             :country,
             :city,
             :address,
             :feed_id,
             # :users,
             :users_sm,
             :program,
             :launch_date,
             :end_date,
             :final_date,
             :is_owner,
             :is_admin,
             :is_member,
             :has_clapped,
             :has_followed,
             :claps_count,
             :follower_count,
             :members_count,
						 :projects_count,
						 :created_at,
						 :updated_at

  attribute :description, :if => :show_objects?
  attribute :description_fr, :if => :show_objects?
  attribute :rules, :if => :show_objects?
  attribute :rules_fr, :if => :show_objects?
  attribute :faq, :if => :show_objects?
  attribute :faq_fr, :if => :show_objects?

  def program
    @program = object.program
    unless @program.nil?
      {
        id: @program.id,
        title: @program.title,
        title_fr: @program.title_fr,
        short_title: @program.short_title
      }
    else
        {
          id: -1,
          title: object.title
        }
    end
  end

  # def projects_count
  #   ChallengesProject.where(challenge_id: object.id, status:"accept").count
  # end


  def show_objects?
     @instance_options[:show_objects]
  end

end
