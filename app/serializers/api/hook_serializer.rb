class Api::HookSerializer < ActiveModel::Serializer
  # include Api::Usersserializerhelper
  # include Api::Utilsserializerhelper
  # include Api::Relationsserializerhelper

  attributes  :id,
              :hook_type,
              :hook_params,
              :trigger_need,
              :trigger_post,
              :trigger_project,
              :trigger_member

  def hook_params
    if object.hook_type == "Slack"
      slackhook = object.slackhooks.first
      {
        hook_url: slackhook[:hook_url],
        channel: slackhook[:channel],
        username: slackhook[:username],
      }
    else
      {error: "Unknown hook type"}
    end
  end
end
