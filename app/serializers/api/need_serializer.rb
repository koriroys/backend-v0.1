class Api::NeedSerializer < ActiveModel::Serializer
  include Api::Usersserializerhelper
  include Api::Utilsserializerhelper
  include Api::Relationsserializerhelper
  include Api::Rolesserializerhelper

  attributes :id,
             :title,
             :content,
             :creator,
             :documents,
             :status,
             :feed_id,
             :users,
             :users_sm,
            #  :end_date,
						 :skills,
             :ressources,
						 :project,
             :is_owner,
             :is_member,
             :has_followed,
             :follower_count,
             :claps_count,
						 :has_clapped,
						 :is_urgent,

  def project
    @project = object.project
    unless @project.nil?
      {
        id: @project.id,
        title: @project.title,
      }
    else
        {
          id: -1,
          title: object.title
        }
    end
  end

end
