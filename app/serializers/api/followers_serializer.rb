class Api::FollowersSerializer < ActiveModel::Serializer
	include Api::Relationsserializerhelper
  attributes :followers

  def followers
    object.followers.map do |user_id|
			if User.exists? user_id.user_id
      	user = User.find(user_id.user_id)
	      if user.avatar.attached?
	        logo_url= Rails.application.routes.url_helpers.rails_blob_url(user.avatar)
	      else
	        logo_url= user.logo_url
	      end
				{
					id: user.id,
					first_name: user.first_name,
					last_name: user.last_name,
					logo_url: logo_url,
					logo_url_sm: user.logo_url_sm,
					has_followed: has_followed(user),
					short_bio: user.short_bio
				}
			else
				{
					id: -1,
					first_name: "User",
					last_name: "Deleted",
					logo_url: "/static/media/default-user-1.2e46be09.png",
					logo_url_sm: "/static/media/default-user-1.2e46be09.png",
					has_followed: false,
					short_bio: ""
				}
			end
    end
  end

end
