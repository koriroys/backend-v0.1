class Api::ProgramSerializer < ActiveModel::Serializer
  include Api::Relationsserializerhelper
  include Api::Rolesserializerhelper
  include Api::Usersserializerhelper
  include Api::Utilsserializerhelper

  attributes :id,
             :title,
             :title_fr,
             :short_title,
             :short_title_fr,
             :banner_url,
             :banner_url_sm,
             :status,
             # :users,
             :users_sm,
             :feed_id,
             :short_description,
             :short_description_fr,
             :faq,
             :faq_fr,
             :enablers,
             :enablers_fr,
             :ressources,
             :launch_date,
             :end_date,
             :is_owner,
             :is_admin,
             :is_member,
             :has_clapped,
             :has_followed,
             :claps_count,
             :follower_count,
             :members_count

    attribute :description, :if => :show_objects?
		attribute :description_fr, :if => :show_objects?
	  attribute :enablers, :if => :show_objects?
	  attribute :enablers_fr, :if => :show_objects?
	  attribute :faq, :if => :show_objects?
	  attribute :faq_fr, :if => :show_objects?
	  attribute :ressources, :if => :show_objects?
	  # attribute :users, :if => :show_objects?

    def show_objects?
       @instance_options[:show_objects]
    end

    def ressources
      object.ressources
    end

end
