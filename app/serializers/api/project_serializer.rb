class Api::ProjectSerializer < ActiveModel::Serializer
  include Api::Relationsserializerhelper
  include Api::Rolesserializerhelper
  include Api::Usersserializerhelper
  include Api::Utilsserializerhelper

  attributes :id,
             :title,
             :short_title,
             :banner_url,
             :banner_url_sm,
             :short_description,
             :creator,
             :status,
             :skills,
             :interests,
             :documents,
             :documents_feed,
             :geoloc,
             :country,
             :city,
             :address,
             :feed_id,
             :is_private,
             :challenges,
             # :users,
             :users_sm,
             :is_owner,
             :is_admin,
             :is_member,
             :has_clapped,
             :has_followed,
             :claps_count,
             :follower_count,
						 :members_count,
						 :needs_count,
             :desc_elevator_pitch,
             :desc_problem_statement,
             :desc_objectives,
             :desc_state_art,
             :desc_progress,
             :desc_stakeholder,
             :desc_impact_strat,
             :desc_ethical_statement,
             :desc_sustainability_scalability,
             :desc_communication_strat,
             :desc_funding,
             :desc_contributing
    attribute :description, :if => :show_objects?
    attribute :desc_elevator_pitch, :if => :show_objects?
    attribute :desc_problem_statement, :if => :show_objects?
    attribute :desc_objectives, :if => :show_objects?
    attribute :desc_state_art, :if => :show_objects?
    attribute :desc_progress, :if => :show_objects?
    attribute :desc_stakeholder, :if => :show_objects?
    attribute :desc_impact_strat, :if => :show_objects?
    attribute :desc_ethical_statement, :if => :show_objects?
    attribute :desc_sustainability_scalability, :if => :show_objects?
    attribute :desc_communication_strat, :if => :show_objects?
    attribute :desc_funding, :if => :show_objects?
    attribute :desc_contributing, :if => :show_objects?
    attribute :documents_feed, :if => :show_objects?

    def show_objects?
       @instance_options[:show_objects]
    end

    def challenges
      object.challenges.select(:id, :title, :short_title, :title_fr, :short_description, :short_description_fr, :banner_url)
    end

    # def members_count
    #    object.users.count rescue 0
    # end
end
