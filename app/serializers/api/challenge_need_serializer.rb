class Api::ChallengeNeedSerializer < ActiveModel::Serializer
  include Api::Relationsserializerhelper
  include Api::Rolesserializerhelper
  include Api::Usersserializerhelper
  include Api::Utilsserializerhelper

  attributes :needs

# Only needed params for needs of a program are returned
  def needs
    result = []
    done = []
    object.projects.map do |project|
      unless done.include? project.id
        project.needs.map do |need|
          result << {
             id: need.id,
             title: need.title,
             content: need.content,
             creator: creator(need),
             documents: need.documents,
             status: need.status,
             feed_id: need.feed.id,
             users: need.users,
  					 skills: need.skills,
  					 project: need_project(need),
             is_owner: is_owner(need),
             is_member: is_member(need),
             has_followed: has_followed(need),
             follower_count: need.follower_count,
             claps_count: need.claps_count,
  					 has_clapped: has_clapped(need),
  					 is_urgent: need.is_urgent,
          }
        end
        done << project.id
      end
    end
    result
  end

  def need_project(need)
    @project = need.project
    unless @project.nil?
      {
        id: @project.id,
        title: @project.title,
      }
    else
        {
          id: -1,
          title: need.title
        }
    end
  end

end
