class Api::CommentSerializer < ActiveModel::Serializer
  include Api::Usersserializerhelper

  attributes :id,
             :content,
             :creator,
             :created_at,
             :claps_count

end
