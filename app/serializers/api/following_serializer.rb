class Api::FollowingSerializer < ActiveModel::Serializer
  has_many :projects, serializer: Api::ProjectSerializer
  has_many :communities, serializer: Api::CommunitySerializer
  has_many :users, serializer: Api::UserSerializer
  has_many :challenges, serializer: Api::ChallengeSerializer


	def projects
		object.following.where(resource_type: "Project").select{ |project| Project.exists? project.resource_id}.map do |project|
			@obj = Project.find(project.resource_id)
		end
  end

  def communities
		object.following.where(resource_type: "Community").select{ |community| Community.exists? community.resource_id}.map do |community|
    	@obj = Community.find(community.resource_id)
    end
  end

  def users
		object.following.where(resource_type: "User").select{ |user| User.exists? user.resource_id}.map do |user|
      @obj = User.find(user.resource_id)
    end
  end
	
  def challenges
		object.following.where(resource_type: "Challenge").select{ |challenge| Challenge.exists? challenge.resource_id}.map do |challenge|
      @obj = Challenge.find(challenge.resource_id)
    end
  end

end
