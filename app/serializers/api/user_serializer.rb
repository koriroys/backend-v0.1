class Api::UserSerializer < ActiveModel::Serializer
  include Api::Relationsserializerhelper
  include Api::Rolesserializerhelper
  include Api::Utilsserializerhelper

	attributes  :id,
              :first_name,
              :last_name,
              :nickname,
              :affiliation,
              :category,
              :country,
              :city,
              :bio,
							:short_bio,
							:can_contact,
              :status,
              :confirmed_at,
              :interests,
              :skills,
              :ressources,
              :feed_id,
              :logo_url,
              :logo_url_sm,
              :claps_count,
              :is_admin,
              :has_clapped,
              :has_followed,
              :follower_count,
              :following_count,
              :geoloc
              # :email


end
