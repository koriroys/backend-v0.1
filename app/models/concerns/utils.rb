module Utils
  # def geocode(object_type)
  #   object_id = self.id
  #   if self.country?
  #     GeocoderWorker.perform_async(object_type, object_id)
  #   end
  # end

  def make_address
    address = [self.address, self.city, self.country].compact.join(', ')
    # address = @object.ip
    if self.class.name == "User"
      if self.city.nil?
          address = self.ip
      end
    end
    return address
  end

  def obj_type
    self.class.name
  end

  def sanitize_description
    self.description = ActionController::Base.helpers.sanitize(
      self.description,
      options = {
        tags: ['p', 'br', 'h1', 'h2', 'a', 'strong', 'em', 'u', 's', 'sub', 'sup', 'span', 'img', 'iframe', 'pre', 'blockquote', 'ul', 'li'],
        attributes: ["height", "width", "style", "src", "href", "class", "allowfullscreen", "frameborder", "data-value", "data-index", "data-denotation-char", "data-link"]
      })
  end

  def sanitize_content
    self.content = ActionController::Base.helpers.sanitize(
      self.content,
      options = {
        tags: ['p', 'br', 'h1', 'h2', 'a', 'strong', 'em', 'u', 's', 'sub', 'sup', 'span', 'img', 'iframe', 'pre', 'blockquote', 'ul', 'li'],
        attributes: ["height", "width", "style", "src", "href", "class", "allowfullscreen", "frameborder", "data-value", "data-index", "data-denotation-char", "data-link"]
      })
  end
end


def users_sm
  results = []
  done = []
  owners = []
  (User.with_role(:owner, self) + User.with_role(:admin, self) + User.with_role(:member, self)).map do |user|
    if results.count < 6 and not user.has_role? :admin
      unless done.include? user.id
        done << user.id
        if user.has_role?(:owner, self)
          owners << user
        end
        results << {
          "id": user.id,
          "first_name": user.first_name,
          "last_name": user.last_name,
          "short_bio": user.short_bio,
          "owner": user.has_role?(:owner, self),
          "admin": user.has_role?(:admin, self),
          "member": user.has_role?(:member, self),
          "logo_url": user.logo_url_sm
        }
      end
    end
  end
  if owners.count == 0
    User.with_role(:admin).map do |user|
      results << {
        "id": user.id,
        "first_name": user.first_name,
        "last_name": user.last_name,
        "short_bio": user.short_bio,
        "owner": user.has_role?(:owner, self),
        "admin": user.has_role?(:admin, self),
        "member": user.has_role?(:member, self),
        "logo_url": user.logo_url_sm
      }
    end
  end
  return results
end

# def banner_url(obj=nil)
# 	if obj.nil?
# 		obj = object
# 	end
# 	if !obj.banner.attachment.nil?
# 		variant = obj.banner.variant(resize: "400x400^")
# 		Rails.application.routes.url_helpers.rails_representation_url(variant)
# 		# Rails.application.routes.url_helpers.rails_blob_url(obj.banner)
# 	else
# 		""
# 	end
# end
