class ChallengesProject < ApplicationRecord
  belongs_to :challenge
	belongs_to :project
	# after_update :notify_admin

  # private
  # def notify_admin
  # 	if status_changed? && status == "accepted"
  # 		NotificationEmailWorker.perform_async(project.creator_id, "Your request for joining the project has been accepted", 'project/' + @project.id.to_s + '/edit', "A project has approved to join your Challenge!")
  # 	end
  # end

end
