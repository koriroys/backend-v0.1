class Ressource < ApplicationRecord
  include AlgoliaSearch
  belongs_to :ressourceable, :polymorphic => true

  algoliasearch disable_indexing: Rails.env.test? do
    attribute :ressource_name
  end
end
