class Post < ApplicationRecord
  # Each post must have a content must belong to user, must belong to a feed.
  # Document restrictions are applied in order to not let a user upload anything
  resourcify
  include RelationHelpers
  include Utils

  belongs_to :user
  has_and_belongs_to_many :feeds
  has_many :comments
  has_many :mentions
  has_many_attached :documents
  # TODO SERIOUSLY FIX THOSE
  # validates :documents, content_type: ["image/png",
  #                                      "image/jpeg",
  #                                      "image/gif",
  #                                      "text/csv",
  #                                      "application/pdf",
  #                                      "image/svg+xml",
  #                                      "application/x-tar",
  #                                      "image/tiff",
  #                                      "application/zip",
  #                                      "application/x-7z-compressed"
  #                                    ]
  # validates :documents, content_size: 31457280  # Document must be less than 30Mb


  validates :content, presence: true

  before_create :sanitize_content
  before_update :sanitize_content
end
