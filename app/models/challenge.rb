class Challenge < ApplicationRecord
  resourcify
  include RelationHelpers
  include AlgoliaSearch
  include Utils


  has_many :challenges_users
  has_many :challenges_projects
  has_many :challenges_communities
  has_many :users, through: :challenges_users
  has_many :projects, through: :challenges_projects
  has_many :communities, through: :challenges_communities
  has_one :feed
  belongs_to :program, optional: true

  has_many :externalhooks, :as => :hookable

  has_and_belongs_to_many :skills
  has_and_belongs_to_many :interests

  has_one_attached :banner
  has_many_attached :documents
  validates :banner, content_type: ["image/png", "image/jpeg", "image/gif"]

  # TODO FIX THE VALIDATION FOR HAS_MANY

  # validates :documents, content_type: ["image/png",
  #                                      "image/jpeg",
  #                                      "image/gif",
  #                                      "text/csv",
  #                                      "application/pdf",
  #                                      "image/svg+xml",
  #                                      "application/x-tar",
  #                                      "image/tiff",
  #                                      "application/zip",
  #                                      "application/x-7z-compressed"
  #                                    ]
  # validates :documents, content_size: 31457280  # Document must be less than 30Mb

  enum status: [:draft, :soon, :active, :completed]

  after_create :create_feed

  before_create :sanitize_description
  before_update :sanitize_description
  after_update :add_user_from_projects

  geocoded_by :make_address
  after_validation :geocode

  public :users_sm

  algoliasearch disable_indexing: Rails.env.test? do
    attribute :id,
              :title,
							:short_description,
							:banner_url,
              :banner_url_sm,
              :interests,
              :skills,
              :status,
              :launch_date,
              :end_date,
              :final_date
    add_attribute :obj_type
    geoloc :latitude, :longitude
  end

  def banner_url_sm
    if self.banner.attachment.nil?
      self.banner_url
    else
      variant = self.banner.variant(resize: "100x100^")
      Rails.application.routes.url_helpers.rails_representation_url(variant)
    end
  end

  def create_feed
    feed = Feed.create({challenge_id: self.id, object_type:'challenge'})
    save
  end

  def add_user_from_projects
    self.projects.map do |project|
      if project_is_confirmed? project
        project.users.map do |user|
          unless self.users.include? user
            self.users << user
            unless user.has_role? :member, self
              user.add_role :member, self
            end
          end
        end
      end
    end
  end

  def project_is_confirmed?(project)
    relation = ChallengesProject.find_by(challenge_id: self.id, project_id: project.id)
    if relation.status == "accept"
      true
    else
      false
    end
  end

  def projects_count
    ChallengesProject.where(challenge_id: self.id, status:"accept").count
  end

end
