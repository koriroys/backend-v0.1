class Mention < ApplicationRecord
  belongs_to :post
  validates_uniqueness_of :post, scope: %i[obj_type obj_id]
end
