class Project < ApplicationRecord
  # Posts must have a unique short name.
  resourcify
  include AlgoliaSearch
  include RelationHelpers
  include Generators
  include Utils

  has_many :users_projects
  has_many :users, through: :users_projects
  has_many :challenges_projects
  has_many :challenges, through: :challenges_projects
  has_many :needs_projects
  has_many :needs, through: :needs_projects
  has_and_belongs_to_many :skills
  has_and_belongs_to_many :interests
  has_one :feed
  belongs_to :creator, foreign_key: "creator_id", class_name: "User"
  has_one_attached :avatar
  has_one_attached :banner
  has_many_attached :documents
  has_many :customfields, :as => :resource

  has_many :externalhooks, :as => :hookable

  validates :avatar, content_type: ["image/png", "image/jpeg", "image/gif"]
  # validates :avatar, content_size: 314573 # Less than 300 Kb per avatar image
  validates :banner, content_type: ["image/png", "image/jpeg", "image/gif"]
  # validates :avatar, content_size: 629145 # Less than 600 Kb per avatar image

  # TODO : FIX THE VALIDATION FOR HAS_MANY

  # validates :documents, content_type: ["image/png",
  #                                      "image/jpeg",
  #                                      "image/gif",
  #                                      "text/csv",
  #                                      "application/pdf",
  #                                      "image/svg+xml",
  #                                      "application/x-tar",
  #                                      "image/tiff",
  #                                      "application/zip",
  #                                      "application/x-7z-compressed"
  #                                    ]
  # validates :documents, content_size: 31457280  # Document must be less than 30Mb


  validates :short_title, uniqueness: true
  validates :title, presence: true, uniqueness: true

  after_create :create_feed

  before_create :create_coordinates
  # before_create :create_avatars

  before_create :sanitize_description
  before_update :sanitize_description

  enum status: [:active, :archived, :draft, :completed]
  # After the model is validated geocode !
  # after_validation :geocode, on: [ :update ]

  geocoded_by :make_address
  after_validation :geocode

  public :users_sm

  algoliasearch disable_indexing: Rails.env.test? do
    attribute :id,
              :title,
              :short_title,
              :short_description,
              :skills,
              :interests,
              :status,
              :banner_url,
              :banner_url_sm,
              :creator,
              :country,
              :city,
              :users_sm,
              :claps_count,
              :follower_count,
              :needs_count,
              :members_count
    geoloc :latitude, :longitude
  end

  def banner_url_sm
    if self.banner.attachment.nil?
      self.banner_url
    else
      variant = self.banner.variant(resize: "100x100^")
      Rails.application.routes.url_helpers.rails_representation_url(variant)
    end
  end

  def needs_count
    self.needs.count
  end

  def members_count
    User.with_role(:member, self).count
  end


  # We need to create feed for each object before creation in order to later have
  # posts in it

  def create_feed
    feed = Feed.create({project_id: self.id, object_type:'project'})
    save
  end

end
