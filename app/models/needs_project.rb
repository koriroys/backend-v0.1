class NeedsProject < ApplicationRecord
  belongs_to :need
  belongs_to :project
end
