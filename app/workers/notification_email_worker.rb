class NotificationEmailWorker
  include Sidekiq::Worker

  def perform(user_id, message, url, title, attachment_path = nil)
    @user = User.find(user_id)
    unless @user.status == 1
      NotificationMailer.notify(@user, message, url, title, attachment_path).deliver
    end
  end
end
