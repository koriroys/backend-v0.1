require "concerns/networks.rb"

class NetworkMakerWorker
  include Sidekiq::Worker

  def add_roles(user, obj)
    @G.add_edge(user, obj, "is_member", true)
    if user.has_role? :admin, obj
      @G.add_edge(user, obj, "is_admin", true)
    end
    if user.has_role? :owner, obj
      @G.add_edge(user, obj, "is_owner", true)
    end
  end

  def add_relations(user, obj)
    if user.follows? obj
      @relation = Relation.find_by(user_id: user.id, resource_type: obj.class.name, resource_id: obj.id)
      @G.add_edge(user, obj, "has_followed", true, metadata:{created_at: @relation.created_at, updated_at: @relation.updated_at})
    end
    if user.clapped? obj
      @relation = Relation.find_by(user_id: user.id, resource_type: obj.class.name, resource_id: obj.id)
      @G.add_edge(user, obj, "has_clapped", true, metadata:{created_at: @relation.created_at, updated_at: @relation.updated_at})
    end
  end

  def add_user_relations(user, obj, relation)
    if user.follows? obj
      @G.add_edge(user, obj, "has_followed", true, metadata:{created_at: relation.created_at, updated_at: relation.updated_at})
    end
    if user.clapped? obj
      @G.add_edge(user, obj, "has_clapped", true, metadata:{created_at: relation.created_at, updated_at: relation.updated_at})
    end
  end

  def add_skills(obj)
    obj.skills.map do |skill|
    @G.add_edge(obj, skill, "has_skill", true)
    end
  end

  def add_ressources(obj)
    obj.ressources.map do |ressource|
      @G.add_edge(obj, ressource, "has_ressource", true)
    end
  end

  def add_interests(obj)
    obj.interests.map do |interest|
      @G.add_edge(obj, interest, "has_interest", true)
    end
  end

  def add_projects(obj)
    obj.projects.map do |project|
      @G.add_edge(obj, project, "has_project", true)
    end
  end

  def add_challenges(obj)
    obj.challenges.map do |challenge|
      @G.add_edge(obj, challenge, "has_challenge", true)
    end
  end

  def perform()
    @G = Graph.new("JOGL database directed graph", "DiGraph", directed=true, metadata={created_at: Time.now})
    # test_graph = Graph.new("JOGL database directed graph", "DiGraph", directed=true, metadata={created_at: Time.now})
    #
    # logger.info(test_graph.name)
    # logger.info(test_graph.make_node_id(User.first))

    # Creating all the nodes
    Skill.all.map do |skill|
      @G.add_node(skill,
                 "Skill",
                 skill.skill_name,
                 metadata: {})
    end

    Ressource.all.map do |ressource|
      @G.add_node(ressource,
                 "Ressource",
                 ressource.ressource_name,
                 metadata: {})
    end

    Interest.all.map do |interest|
      @G.add_node(interest,
                 "Interest",
                 interest.interest_name,
                 metadata: {})
    end

    User.all.map do |user|
      @G.add_node(user,
                 "User",
                 user.nickname,
                 metadata: {
                   first_name: user.first_name,
                   last_name: user.last_name,
                   status: user.status
                   })
      add_skills(user)
      add_ressources(user)
      add_interests(user)
    end




    Relation.where(resource_type: "User").map do |relation|
      user1 = User.find_by(id: relation.user_id)
      user2 = User.find_by(id: relation.resource_id)
      unless user2.nil? or user1.nil?
        add_user_relations(user1, user2, relation)
      end
    end

    Post.all.map do |post|
      @G.add_node(post,
                 "Post",
                 post.id,
                 metadata: {created_at: post.created_at, updated_at: post.updated_at})
      user = User.find_by(id: post.user_id)
      unless user.nil?
        @G.add_edge(user, post, "is_author", true)
      end
      post.mentions.map do |mention|
        obj = mention[:obj_type].humanize.constantize.find_by(id: mention[:obj_id])
        unless obj.nil?
          @G.add_edge(post, obj, "has_mentioned", true, "", {match: mention.obj_match})
        end
      end
      post.comments.map do |comment|
        @G.add_node(comment,
                   "Comment",
                   comment.id,
                   metadata: {created_at: comment.created_at, updated_at: comment.updated_at})
        user = User.find_by(id: comment.user_id)
        unless user.nil?
          @G.add_edge(user, comment, "is_author", true)
        end
        @G.add_edge(post, comment, "is_commented_by", true)
      end
    end

    Need.all.map do |need|
      @G.add_node(need,
                 "Need",
                 need.title,
                 metadata: {})
      add_skills(need)
      add_ressources(need)
      need.users.map do |user|
        add_roles(user, need)
        add_relations(user, need)
      end
    end

    Project.all.map do |project|
      @G.add_node(project,
                 "Project",
                 project.title,
                 metadata: {
                   short_title: project.short_title,
                   short_description: project.short_description,
                   status: project.status
                   })
      add_skills(project)
      add_interests(project)
      project.users.map do |user|
        add_roles(user, project)
        add_relations(user, project)
      end
    end

    Community.all.map do |community|
      @G.add_node(community,
                 "Community",
                 community.title,
                 metadata: {
                   short_title: community.short_title,
                   short_description: community.short_description,
                   status: community.status
                   })
      add_skills(community)
      add_ressources(community)
      add_interests(community)
      community.users.map do |user|
       add_roles(user, community)
       add_relations(user, community)
      end
    end

    Challenge.all.map do |challenge|
      @G.add_node(challenge,
                 "Challenge",
                 challenge.title,
                 metadata: {
                   short_title: challenge.short_title,
                   short_description: challenge.short_description,
                   status: challenge.status
                   })
      add_skills(challenge)
      add_interests(challenge)
      add_projects(challenge)
      challenge.users.map do |user|
        add_roles(user, challenge)
        add_relations(user, challenge)
      end
    end

    Program.all.map do |program|
      @G.add_node(program,
                 "Program",
                 program.title,
                 metadata: {
                   short_title: program.short_title,
                   short_description: program.short_description,
                   status: program.status
                   })
      add_challenges(program)
      program.users.map do |user|
        add_roles(user, program)
        add_relations(user, program)
      end
    end


    # @G.render
    network = Network.create(json_network: @G.render)
    network.save!
  end
end
