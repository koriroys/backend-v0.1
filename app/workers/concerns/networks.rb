class Graph
  attr_accessor :name, :type, :directed, :metadata, :nodes, :edges

  def initialize(name, type, directed=false, metadata={})
    @name = name
    @type = type
    @directed = directed
    @metadata = metadata
    @nodes = {}
    @edges = []
  end

  def make_node_id(node)
    node.class.name + "_" + node.id.to_s
  end

  def add_edge(node1, node2, relation="", directed=false, label="", metadata={})
      edge = {
        source: make_node_id(node1),
        relation: relation,
        target: make_node_id(node2),
        directed: directed,
        label: label,
        metadata: metadata
      }
      @edges << edge
  end

  def add_node(node, type, label, metadata)
    @nodes[make_node_id(node)] = {
      type: type,
      label: label,
      metadata: metadata
    }
  end

  def render
    {
      graph: {
        directed: @directed,
        type: @type,
        label: @label,
        metadata: @metadata,
        nodes: @nodes,
        edges: @edges
      }
    }
  end

end
