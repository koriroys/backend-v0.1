class CleanDeadUsers
  include Sidekiq::Worker

  def perform
    User.all.map do |user|
      unless user.confirmed?
				dt = (Time.now - user.created_at) / 1.hours
				# if user has been created more than 30 days ago, delete it
        if dt > 720
          user.destroy
        end
      end
    end
  end
end
