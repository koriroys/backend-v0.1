class InviteUserMailer < ApplicationMailer

  def invite_user(owner, object, joiner)
    @owner = owner
		@object = object
		@from_object = object.class.name.downcase != "community" ? object.class.name.downcase : "group"
    @joiner = joiner
    mail(:to => "<#{joiner.email}>", :subject => "You have been invited to join a " + @from_object)
  end
end
