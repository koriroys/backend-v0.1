class NotificationMailer < ApplicationMailer

  def notify(user, message, url, title, attachment_path)
  	attachments['participation_rules.pdf'] = File.read(attachment_path) if attachment_path.present?
    @user = user
    @message = message
    @url = url
    @title = title
    mail(:to => "<#{user.email}>", :subject => "#{title}")
  end
end
