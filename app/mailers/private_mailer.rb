class PrivateMailer < ApplicationMailer

  def send_private_email(from, to, object, content)
    @from = from
    @object = object
    @to = to
    @content = content
		mail(:to => "#{@to.first_name} #{@to.last_name} <#{@to.email}>",
		     :reply_to => "#{@from.first_name} #{@from.last_name} <#{@from.email}>",
		     :subject => "New message from #{@from.first_name} #{@from.last_name}")
	end
end
