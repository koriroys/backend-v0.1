class UserJoinedMailer < ApplicationMailer

  def user_joined(owner, object, joiner)
    @owner = owner
		@object = object
		@from_object = object.class.name.downcase != "community" ? object.class.name.downcase : "group"
    @joiner = joiner
    mail(:to => "<#{owner.email}>", :subject => "A user joined your " + @from_object)
  end
end