FactoryBot.define do
  factory :program do
    description { "MyString" }
    title {FFaker::Movie.title}
    short_title {FFaker::Internet.user_name}
    short_description {FFaker::Movie.title}
  end
end
