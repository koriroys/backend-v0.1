FactoryBot.define do
  factory :skill do
    skill_name { FFaker::Name.name }
  end
end
