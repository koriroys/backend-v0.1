FactoryBot.define do
  factory :ressource do
    ressourceable_id { 1 }
    ressourceable_type { "User" }
    ressource_name { FFaker::Name.name }
  end
end
