FactoryBot.define do
  factory :challenge do
    title { FFaker::Name.name }
    description { "Challenge's Description" }
    short_description { "challenge's short description" }

    # after :create do |challenge|
    #   skills = FactoryBot.create_list(:skill, 5)
    #   skills.map do |skill|
    #     challenge.skills << skill   # has_many
    #   end
    # end
  end
end
