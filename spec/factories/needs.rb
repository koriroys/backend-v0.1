FactoryBot.define do
  factory :need do
  	project
  	user
  	title {FFaker::Name.first_name}
  	content {FFaker::DizzleIpsum.paragraph}
  	status {0}

    after :create do |need|
      skills = FactoryBot.create_list(:skill, 5)
      skills.map do |skill|
        need.skills << skill   # has_many
      end
    end

    after :create do |need|
      ressources = FactoryBot.create_list(:ressource, 5, ressourceable_id: need.id, ressourceable_type: "Need")
      ressources.map do |ressource|
        need.ressources << ressource   # has_many
      end
    end
  end
end
