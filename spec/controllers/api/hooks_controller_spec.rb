require 'rails_helper'

RSpec.describe Api::ProjectsController, type: :controller do
  before (:each) do
    @user = User.create!({
      email: 'user@test.com',
      password: 'password123',
      password_confirmation: 'password123'
      })
    @user.confirm
    sign_in @user
    allow_any_instance_of(Api::UsersController).to receive(:current_user).and_return @user
  end

  describe '#hooks_project' do
    before (:each) do
      @project = FactoryBot.create(:project, creator_id: @user.id)
    end

    it 'should create a hook' do
      @user.add_role :admin, @project
      post :create_external_hook, params: {id: @project.id, hook: {hook_type: "Slack"}}
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 201
      expect(@project.externalhooks.count).to eq 1
    end

    it 'should not create a hook if not admin' do
      post :create_external_hook, params: {id: @project.id, hook: {hook_type: "Slack"}}
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 403
      expect(@project.externalhooks.count).to eq 0
    end

    it 'should not create a hook if not signed_in' do
      sign_out @user
      post :create_external_hook, params: {id: @project.id, hook: {hook_type: "Slack"}}
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 401
      expect(@project.externalhooks.count).to eq 0
    end

    it 'should update a hook' do
      @user.add_role :admin, @project
      hook = @project.externalhooks.create(hook_type: "Other")
      patch :update_external_hook, params: {
                                          id: @project.id,
                                          hook_id: hook.id,
                                          hook: {
                                            hook_type: "Slack",
                                            trigger_need: true,
                                            trigger_post: true,
                                            trigger_member: true,
                                            trigger_project: true,
                                            hook_params: {
                                              hook_url: "something",
                                              channel: "chan",
                                              username: "some"
                                              }
                                            }
                                          }
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(@project.externalhooks.first.hook_type).to match("Slack")
    end

    it 'should NOT update a hook if NOT admin' do
      hook = @project.externalhooks.create(hook_type: "Other")
      patch :update_external_hook, params: {
                                          id: @project.id,
                                          hook_id: hook.id,
                                          hook: {
                                            hook_type: "Slack",
                                            "trigger_need": true,
                                            "trigger_post": true,
                                            "trigger_member": true,
                                            "trigger_project": true,
                                            hook_params: {
                                              hook_url: "something",
                                              channel: "chan",
                                              username: "some"
                                              }
                                            }
                                          }
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 403
      expect(@project.externalhooks.first.hook_type).to match("Other")
    end

    it 'should NOT update a hook if NOT signedin' do
      sign_out @user
      hook = @project.externalhooks.create(hook_type: "Other")
      patch :update_external_hook, params: {
                                          id: @project.id,
                                          hook_id: hook.id,
                                          hook: {
                                            hook_type: "Slack",
                                            "trigger_need": true,
                                            "trigger_post": true,
                                            "trigger_member": true,
                                            "trigger_project": true,
                                            hook_params: {
                                              hook_url: "something",
                                              channel: "chan",
                                              username: "some"
                                              }
                                            }
                                          }
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 401
      expect(@project.externalhooks.first.hook_type).to match("Other")
    end

    it 'should delete a hook' do
      @user.add_role :admin, @project
      hook = @project.externalhooks.create(hook_type: "Other")
      delete :delete_external_hook, params: {id: @project.id, hook_id: hook.id}
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(@project.externalhooks.count).to eq 0
    end

    it 'should not delete a hook if not admin' do
      hook = @project.externalhooks.create(hook_type: "Other")
      delete :delete_external_hook, params: {id: @project.id, hook_id: hook.id}
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 403
      expect(@project.externalhooks.count).to eq 1
    end

    it 'should not delete a hook if not signed_in' do
      hook = @project.externalhooks.create(hook_type: "Other")
      sign_out @user
      delete :delete_external_hook, params: {id: @project.id, hook_id: hook.id}
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 401
      expect(@project.externalhooks.count).to eq 1
    end

    it 'should get a hook' do
      @user.add_role :admin, @project
      hook = @project.externalhooks.create(hook_type: "Other")
      hook2 = @project.externalhooks.create(hook_type: "Other")
      get :get_external_hooks, params: {id: @project.id}
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(@project.externalhooks.count).to eq 2
      expect(json_response.count).to eq 2
    end

    it 'should not get a hook if not admin' do
      hook = @project.externalhooks.create(hook_type: "Other")
      hook2 = @project.externalhooks.create(hook_type: "Other")
      get :get_external_hooks, params: {id: @project.id}
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 403
    end

    it 'should not get a hook if not signed_in' do
      hook = @project.externalhooks.create(hook_type: "Other")
      hook2 = @project.externalhooks.create(hook_type: "Other")
      sign_out @user
      get :get_external_hooks, params: {id: @project.id}
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 401
    end

  end

end



RSpec.describe Api::ChallengesController, type: :controller do
  before (:each) do
    @user = User.create!({
      email: 'user@test.com',
      password: 'password123',
      password_confirmation: 'password123'
      })
    @user.confirm
    sign_in @user
    allow_any_instance_of(Api::UsersController).to receive(:current_user).and_return @user
  end

  describe '#hooks_challenge' do
    before (:each) do
      @challenge = FactoryBot.create(:challenge)
      @challenge.users << @user
    end

    it 'should create a hook' do
      @user.add_role :admin, @challenge
      post :create_external_hook, params: {id: @challenge.id, hook: {hook_type: "Slack"}}
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 201
      expect(@challenge.externalhooks.count).to eq 1
    end

    it 'should not create a hook if not admin' do
      post :create_external_hook, params: {id: @challenge.id, hook: {hook_type: "Slack"}}
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 403
      expect(@challenge.externalhooks.count).to eq 0
    end

    it 'should not create a hook if not signed_in' do
      sign_out @user
      post :create_external_hook, params: {id: @challenge.id, hook: {hook_type: "Slack"}}
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 401
      expect(@challenge.externalhooks.count).to eq 0
    end

    it 'should update a hook' do
      @user.add_role :admin, @challenge
      hook = @challenge.externalhooks.create(hook_type: "Other")
      patch :update_external_hook, params: {
                                          id: @challenge.id,
                                          hook_id: hook.id,
                                          hook: {
                                            hook_type: "Slack",
                                            trigger_need: true,
                                            trigger_post: true,
                                            trigger_member: true,
                                            trigger_project: true,
                                            hook_params: {
                                              hook_url: "something",
                                              channel: "chan",
                                              username: "some"
                                              }
                                            }
                                          }
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(@challenge.externalhooks.first.hook_type).to match("Slack")
    end

    it 'should NOT update a hook if NOT admin' do
      hook = @challenge.externalhooks.create(hook_type: "Other")
      patch :update_external_hook, params: {
                                          id: @challenge.id,
                                          hook_id: hook.id,
                                          hook: {
                                            hook_type: "Slack",
                                            "trigger_need": true,
                                            "trigger_post": true,
                                            "trigger_member": true,
                                            "trigger_project": true,
                                            hook_params: {
                                              hook_url: "something",
                                              channel: "chan",
                                              username: "some"
                                              }
                                            }
                                          }
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 403
      expect(@challenge.externalhooks.first.hook_type).to match("Other")
    end

    it 'should NOT update a hook if NOT signedin' do
      sign_out @user
      hook = @challenge.externalhooks.create(hook_type: "Other")
      patch :update_external_hook, params: {
                                          id: @challenge.id,
                                          hook_id: hook.id,
                                          hook: {
                                            hook_type: "Slack",
                                            "trigger_need": true,
                                            "trigger_post": true,
                                            "trigger_member": true,
                                            "trigger_project": true,
                                            hook_params: {
                                              hook_url: "something",
                                              channel: "chan",
                                              username: "some"
                                              }
                                            }
                                          }
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 401
      expect(@challenge.externalhooks.first.hook_type).to match("Other")
    end

    it 'should delete a hook' do
      @user.add_role :admin, @challenge
      hook = @challenge.externalhooks.create(hook_type: "Other")
      delete :delete_external_hook, params: {id: @challenge.id, hook_id: hook.id}
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(@challenge.externalhooks.count).to eq 0
    end

    it 'should not delete a hook if not admin' do
      hook = @challenge.externalhooks.create(hook_type: "Other")
      delete :delete_external_hook, params: {id: @challenge.id, hook_id: hook.id}
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 403
      expect(@challenge.externalhooks.count).to eq 1
    end

    it 'should not delete a hook if not signed_in' do
      hook = @challenge.externalhooks.create(hook_type: "Other")
      sign_out @user
      delete :delete_external_hook, params: {id: @challenge.id, hook_id: hook.id}
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 401
      expect(@challenge.externalhooks.count).to eq 1
    end

    it 'should get a hook' do
      @user.add_role :admin, @challenge
      hook = @challenge.externalhooks.create(hook_type: "Other")
      hook2 = @challenge.externalhooks.create(hook_type: "Other")
      get :get_external_hooks, params: {id: @challenge.id}
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(@challenge.externalhooks.count).to eq 2
      expect(json_response.count).to eq 2
    end

    it 'should not get a hook if not admin' do
      hook = @challenge.externalhooks.create(hook_type: "Other")
      hook2 = @challenge.externalhooks.create(hook_type: "Other")
      get :get_external_hooks, params: {id: @challenge.id}
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 403
    end

    it 'should not get a hook if not signed_in' do
      hook = @challenge.externalhooks.create(hook_type: "Other")
      hook2 = @challenge.externalhooks.create(hook_type: "Other")
      sign_out @user
      get :get_external_hooks, params: {id: @challenge.id}
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 401
    end

  end

end
