require 'rails_helper'

RSpec.describe Api::ProjectsController, type: :controller do
  before (:each) do
    @user = User.create!({
      email: 'user@test.com',
      password: 'password123',
      password_confirmation: 'password123'
      })
    @user.confirm
    sign_in @user
    allow_any_instance_of(Api::UsersController).to receive(:current_user).and_return @user
    @project = FactoryBot.create(:project, creator_id: @user.id)
    @user2 = FactoryBot.create(:confirmed_user)
    @user3 = FactoryBot.create(:confirmed_user)
    @project.users << @user
    @project.users << @user2
    @project.users << @user3
    @user.add_role :member, @project
    @user2.add_role :member, @project
    @user3.add_role :member, @project
  end

  describe '#members_list' do
    it 'should return the member list' do
      get :members_list, params: {id: @project.id}
      json_response = JSON.parse(response.body)
      expect(response).to have_http_status :ok
      expect(json_response["users"].count).to eq 3
    end
  end
end


#TODO ADD SPECS FOR MEMBERS HANDLING HERE FOR ALL OBJECTS
