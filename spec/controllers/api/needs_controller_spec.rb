require 'rails_helper'

RSpec.describe Api::NeedsController, type: :controller do
  before (:each) do
    @user = User.create!({
      email: 'user@test.com',
      password: 'password123',
      password_confirmation: 'password123'
      })
    @user.confirm
    sign_in @user
    allow_any_instance_of(Api::UsersController).to receive(:current_user).and_return @user
    @project = FactoryBot.create(:project, creator_id: @user.id)
    @need = FactoryBot.create(:need, project_id: @project.id, user_id: @user.id)
  end

  describe '#index' do
    it 'should return list of need' do
      get :index
      need = JSON.parse(response.body)
      expect(response).to have_http_status :ok
    end
  end

  describe '#create' do
    context 'Decline because the user is not member of project' do
      it 'creates a need' do
        post :create, params: { need: @need.attributes }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 403
      end
    end

    context 'creating need with logged in user and is member of project' do
      it 'creates a need' do
        @user.add_role :member, @project
        post :create, params: { need: @need.attributes }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 201
      end
    end

    context 'creating need without logged in user' do
      it 'returns unauthorized error' do
        @user.add_role :member, @project
        sign_out @user
        post :create, params: { need: @need.attributes }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 401
      end
    end
  end

  describe '#show' do
    it 'should return need' do
      get :show, params: { id: @need.id }
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 200
    end
  end

  describe '#update' do
    context 'Updating need with logged in user with owner role of need' do
      it 'Updates a need' do
        @user.add_role :owner, @need
        put :update, params: { need: { content: "New content", skills: ["Big Data"], ressources: ["3D printers"]}, id: @need.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(json_response["data"]).to eq 'Need has been updated'
        expect(@need.skills.count).to eq 1
        expect(@need.ressources.count).to eq 1
        expect(@need.ressources.first.ressource_name).to match("3D printers")
      end
    end

    context 'Updating need without logged in user' do
      it 'should not update the need' do
        sign_out @user
        put :update, params: { need: { content: "New content"}, id: @need.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 401
      end
    end

    context 'User not authorized for updating need' do
      it 'Should throw forbidden error' do
        put :update, params: { need: { content: "New content"}, id: @need.id  }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 403
      end
    end
  end

  describe '#destroy' do
    context 'Deleting need with user not having owner role of need' do
      it 'Should not dlete the need' do
        delete :destroy, params: { id: @need.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 403
      end
    end

    context 'deleting need without logged in user' do
      it 'Should not Delete the need' do
        sign_out @user
        delete :destroy, params: { id: @need.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 401
      end
    end

    context 'deleting need with logged in user with owner role' do
      it 'Should Delete the need' do
        @user.add_role :owner, @need
        delete :destroy, params: { id: @need.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(json_response["data"]).to eq 'Your need has been deleted'
      end
    end
  end
end
