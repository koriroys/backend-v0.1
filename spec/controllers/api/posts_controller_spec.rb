require 'rails_helper'

RSpec.describe Api::PostsController, type: :controller do
  before (:each) do
    @user = User.create!({
      email: 'user@test.com',
      password: 'password123',
      password_confirmation: 'password123'
      })
    @user.confirm
    sign_in @user
    allow_any_instance_of(Api::UsersController).to receive(:current_user).and_return @user
    @project = FactoryBot.create(:project, creator_id: @user.id)
    @need = FactoryBot.create(:need, project_id: @project.id, user_id: @user.id)
    @feed = FactoryBot.create(:feed, object_type: 'need', need_id: @need.id)
    @post = FactoryBot.create(:post, feed_id: @feed.id, user_id: @user.id, from_object: "User", from_id: @user.id)
  end

  describe '#create' do
    context 'Unauthorized' do
      it "Should Return Unauthorized error because user's not signed in" do
        sign_out @user
        post :create, params: { post: @post.attributes }
        expect(response.status).to eq 401
      end
    end

    context 'creating post' do
      it 'creates a post' do
        post :create, params: { post: @post.attributes }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 201
        expect(json_response["data"]).to eq 'Your post has been published'
      end
    end

  end

  describe '#show' do
    it 'should return post' do
      get :show, params: { id: @post.id }
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 200
    end
  end

  describe '#update' do
    context 'Updating post' do
      it 'Updates a post' do
        put :update, params: { post: { content: "New content"}, id: @post.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(json_response["data"]).to eq 'Post has been updated'
      end
    end
    context 'User not logged in' do
      it 'Will throw error' do
        sign_out @user
        put :update, params: { post: { content: "New content"}, id: @post.id  }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 401
      end
    end
  end

  
  describe '#destroy' do
    it 'Should Delete the post' do
      delete :destroy, params: { id: @post.id }
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(json_response["data"]).to eq 'Your post has been deleted'
    end
    context 'User not logged in' do
      it 'Should not Delete the post' do
        sign_out @user
        delete :destroy, params: { id: @post.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 401
      end
    end
  end

  describe '#comment' do
    context 'with logged in user' do
      it 'create comment' do
        post :comment, params: {id: @post.id, comment: {content: "New Comment"}}
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 201
        expect(json_response["data"]).to eq 'Your comment has been published'
      end
    end

    context 'without logged in user' do
      it 'should not create a comment on a post' do
        sign_out @user
        post :comment, params: {id: @post.id, comment: {content: "New Comment"}}
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 401
      end
    end

    context 'with logged in user' do
      it 'should update comment' do
        comment = FactoryBot.create(:comment, post: @post, user: @user)
        @post.comments << comment
        patch :comment, params: {id: @post.id, comment: {content: "New Comment content"}, comment_id: comment.id}
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(json_response["data"]).to eq 'Your post has been updated'
      end
    end

    context 'without logged in user' do
      it 'should not update comment' do
        comment = FactoryBot.create(:comment, post: @post, user: @user)
        @post.comments << comment
        sign_out @user
        patch :comment, params: {id: @post.id, comment: {content: "New Comment content"}, comment_id: comment.id}
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 401
      end
    end

    context 'logged in user' do
      it 'should remove a comment from a post' do
        comment = FactoryBot.create(:comment, post: @post, user: @user)
        @post.comments << comment
        delete :comment, params: {id: @post.id, comment_id: comment.id}
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(json_response["data"]).to eq 'Comment deleted'
      end
    end

    context 'without logged in user' do
      it 'should not remove a comment from a post' do
        comment = FactoryBot.create(:comment, post: @post, user: @user)
        @post.comments << comment
        sign_out @user
        delete :comment, params: {id: @post.id, comment_id: comment.id}
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 401
      end
    end
  end
end