require 'rails_helper'

RSpec.describe Api::FeedsController, type: :controller do
  before (:each) do
    @user = User.create!({
      email: 'user@test.com',
      password: 'password123',
      password_confirmation: 'password123'
      })
    @user.confirm
    sign_in @user
    allow_any_instance_of(Api::UsersController).to receive(:current_user).and_return @user
    @project = FactoryBot.create(:project, creator_id: @user.id)
    @feed = FactoryBot.create(:feed, object_type: "project", project_id: @project.id)
  end
  describe '#index' do
    context 'with logged in user' do
      it 'should return list of feed' do
        get :index
        feed = JSON.parse(response.body)
        expect(response).to have_http_status :ok
      end
    end
    context 'with logged out  user' do
      it 'should not return list of feed' do
        sign_out @user
        get :index
        feed = JSON.parse(response.body)
        expect(response).to have_http_status :unauthorized
      end
    end
  end

  describe '#show' do
    it 'should return feed' do
      get :show, params: { id: @feed.id }
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 200
    end
  end

  describe '#remove_post' do

    before(:each) do
      @user2 = FactoryBot.create(:confirmed_user)
      @post = FactoryBot.create(:post, user_id: @user2.id)
      @project.feed.posts << @post
    end

    it 'should remove a post from a feed' do
      @user.add_role :admin, @project
      delete :remove_post, params: { id: @project.feed.id, post_id: @post.id }
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(@project.feed.posts.count).to eq 0
    end

    it 'should be forbidden to not admin' do
      delete :remove_post, params: { id: @project.feed.id, post_id: @post.id }
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 403
      expect(@project.feed.posts.count).to eq 1
    end
  end
end
