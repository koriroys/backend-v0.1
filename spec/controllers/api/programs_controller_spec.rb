require 'rails_helper'

RSpec.describe Api::ProgramsController, type: :controller do
  before (:each) do
    @user = User.create!({
      email: 'user@test.com',
      password: 'password123',
      password_confirmation: 'password123'
      })
    @user.confirm
    sign_in @user
    allow_any_instance_of(Api::UsersController).to receive(:current_user).and_return @user
    @program = FactoryBot.create(:program)
  end

  describe '#index' do
    it 'should return list of program' do
      get :index
      program = JSON.parse(response.body)
      expect(response).to have_http_status :ok
    end
  end

  describe '#my_programs' do
    context 'getting users challenges with logged in user' do
      it 'should return list of users Challenges' do
        get :my_programs
        challenges = JSON.parse(response.body)
        expect(response).to have_http_status :ok
      end
    end

    context 'getting users challenges without logged in user' do
      it 'should return list of users Challenges' do
        sign_out @user
        get :my_programs
        challenges = JSON.parse(response.body)
        expect(response).to have_http_status :unauthorized
      end
    end
  end

  describe '#create' do
    context 'without logged in user' do
      it "Return Unauthorized because user's not signed in" do
        sign_out @user
        post :create, params: { program: @program.attributes }
        expect(response.status).to eq 401
      end
    end

    context 'creating program with unapproved user' do
      it 'returns forbidden' do
        @program.short_title = FFaker::Name.first_name
        post :create, params: { program: @program.attributes }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 403
        # expect(json_response["data"]).to eq 'Success'
      end
    end

    context 'creating program with approved user' do
      it 'creates a program' do
        @program.short_title = FFaker::Name.first_name
        @user.add_role :program_creator
        post :create, params: { program: @program.attributes }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 201
        expect(json_response["data"]).to eq 'Success'
      end
    end

  end

  describe '#show' do
    it 'should return program' do
      get :show, params: { id: @program.id }
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 200
    end
  end


  describe '#update' do
    context 'Updating program' do
      it 'Updates a program' do
        @user.add_role :admin
        put :update, params: { program: { description: "New description"}, id: @program.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
      end
    end
    context 'User not logged in' do
      it "Should throw Unauthorized error and won't update the program" do
        sign_out @user
        put :update, params: { program: { description: "New description"}, id: @program.id  }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 401
      end
    end
  end

  describe '#destroy' do
    context 'User not logged in' do
      it 'Should Not Delete the program' do
        sign_out @user
        delete :destroy, params: { id: @program.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 401
      end
    end

    context 'User not logged in' do
      it 'Should Not Delete the program' do
        delete :destroy, params: { id: @program.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
      end
    end
  end

  describe '#attach' do
    context 'attaching a challenge to program with logged in user' do
      it 'should attach a challenge to a program' do
        challenge = FactoryBot.create(:challenge)
        put :attach, params: {id: @program.id, challenge_id: challenge.id}
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(json_response["data"]).to eq 'Challenge attached'
      end
    end
  end

  describe '#get_challenges' do
    context 'we can get the list of challenges attached to a program' do
      it 'should return the list of challenges' do
        challenge1 = FactoryBot.create(:challenge)
        challenge2 = FactoryBot.create(:challenge)
        @program.challenges << challenge1
        @program.challenges << challenge2
        get :index_challenges, params: { id: @program.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(json_response["challenges"].count).to eq 2
      end
    end
  end

  describe '#get_projects' do
    context 'we can get the list of projects attached to a program' do
      it 'should return the list of projects' do
        challenge1 = FactoryBot.create(:challenge)
        challenge2 = FactoryBot.create(:challenge)
        project1 = FactoryBot.create(:project, creator_id: @user.id)
        project2 = FactoryBot.create(:project, creator_id: @user.id)
        project3 = FactoryBot.create(:project, creator_id: @user.id)

        challenge1.projects << project1
        @program.challenges << challenge1
        @program.challenges << challenge2
        challenge1.projects << project2
        challenge2.projects << project3
        challenge2.projects << project1
        get :index_projects, params: { id: @program.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(json_response["projects"].count).to eq 3
      end
    end
  end

  describe '#get_needs' do
    context 'we can get the list of needs attached to a program' do
      it 'should return the list of needs' do
        challenge1 = FactoryBot.create(:challenge)
        challenge2 = FactoryBot.create(:challenge)
        project1 = FactoryBot.create(:project, creator_id: @user.id)
        project2 = FactoryBot.create(:project, creator_id: @user.id)
        project3 = FactoryBot.create(:project, creator_id: @user.id)
        project1.needs << FactoryBot.create(:need, project_id: project1.id, user_id: @user.id)
        project1.needs << FactoryBot.create(:need, project_id: project1.id, user_id: @user.id)
        project2.needs << FactoryBot.create(:need, project_id: project2.id, user_id: @user.id)
        project3.needs << FactoryBot.create(:need, project_id: project3.id, user_id: @user.id)
        challenge1.projects << project1
        @program.challenges << challenge1
        @program.challenges << challenge2
        challenge1.projects << project2
        challenge2.projects << project3
        challenge2.projects << project1
        get :index_needs, params: { id: @program.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(json_response["needs"].count).to eq 4
      end
    end
  end

  describe '#remove' do
    context 'removing a challenge from program with logged in user' do
      it 'should remove a challenge from a program' do
        challenge = FactoryBot.create(:challenge)
        @program.challenges << challenge
        delete :remove, params: {id: @program.id, challenge_id: challenge.id}
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(json_response["data"]).to eq 'Challenge removed'
      end
    end
  end
end
