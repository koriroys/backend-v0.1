require 'rails_helper'

RSpec.describe Api::CommunitiesController, type: :controller do
  before (:each) do
    @user = User.create!({
      email: 'user@test.com',
      password: 'password123',
      password_confirmation: 'password123'
      })
    @user.confirm
    sign_in @user
    allow_any_instance_of(Api::UsersController).to receive(:current_user).and_return @user
    @community = Community.create(
      title: FFaker::Movie.title,
      short_title: FFaker::Internet.user_name,
      logo_url: FFaker::Avatar.image,
      description: FFaker::DizzleIpsum.paragraphs,
      short_description: FFaker::DizzleIpsum.paragraph,
      latitude: rand(-90.000000000...90.000000000),
      longitude: rand(-180.000000000...180.000000000),
      status: 0,
      is_private: true,
      creator_id: @user.id)
  end

  describe '#index' do
    it 'should return list of community' do
      get :index
      community = JSON.parse(response.body)
      expect(response).to have_http_status :ok
    end
  end

  describe '#my communities' do
    context 'Fetching My communites with logged in user' do
      it 'should return list of Users community' do
        get :my_communities
        community = JSON.parse(response.body)
        expect(response).to have_http_status :ok
      end
    end

    context 'Fetching My communites without logged in user' do
      it 'should return list of Users community' do
        sign_out @user
        get :my_communities
        community = JSON.parse(response.body)
        expect(response).to have_http_status :unauthorized
      end
    end
  end

  describe '#create' do
    context 'duplicate short title error' do
      it 'creates a community' do
        post :create, params: { community: @community.attributes }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 422
      end
    end

    context 'creating community with logged in user' do
      it 'creates a community' do
        @community.short_title = FFaker::Name.first_name
        post :create, params: { community: @community.attributes }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 201
      end
    end

    context 'creating community without logged in user' do
      it 'return unauthorized error' do
        @community.short_title = FFaker::Name.first_name
        sign_out @user
        post :create, params: { community: @community.attributes }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 401
      end
    end
  end

  describe '#show' do
    it 'should return community' do
      get :show, params: { id: @community.id }
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 200
    end
  end

  describe '#update' do
    context 'Updating community with logged in user and with admin role' do
      it 'Updates a community' do
        @user.add_role :admin
        put :update, params: { community: { description: "New Description"}, id: @community.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
      end
    end

    context 'User cant update because user is not an admin' do
      it 'Should throw Forbidden error' do
        put :update, params: { community: { description: "New Description"}, id: @community.id  }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 403
      end
    end

    context 'without logged in user' do
      it 'Should throw unauthorized error' do
        sign_out @user
        put :update, params: { community: { description: "New Description"}, id: @community.id  }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 401
      end
    end
  end

  describe '#destroy' do
    context 'with logged in user' do
      it 'Should Delete the community' do
        delete :destroy, params: { id: @community.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
      end
    end

    context 'without logged in user' do
      it 'Should not delete the community' do
        sign_out @user
        delete :destroy, params: { id: @community.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 401
      end
    end
  end
end
