require 'rails_helper'

RSpec.describe Api::UsersController, type: :controller do
  before (:each) do
    @user = User.create!({
      email: 'user@test.com',
      password: 'password123',
      password_confirmation: 'password123'
      })
    @user.confirm
    sign_in @user
    allow_any_instance_of(Api::UsersController).to receive(:current_user).and_return @user
  end

  describe '#index' do
    it 'should return list of users' do
      get :index
      users = JSON.parse(response.body)
      expect(response).to have_http_status :ok
      expect(users.count).to eq 1
    end
  end

  describe '#create' do
    context 'user not exists' do
      let!(:user_params) do
        {
          email: 'user2@test.com',
          password: '12345678',
          nickname: 'testuser'
        }
      end

      it 'gives error password confirmation does not match' do
        post :create, params: { user: user_params }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 422
        expect(json_response["error"]).to eq("Password confirmation does not match the password")
      end

      it 'should create the user if password_confirmation matched' do
        post :create, params: { user: user_params.merge(password_confirmation: '12345678') }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 201
        expect(json_response["msg"]).to eq("Thank you for signing up, please confirm your email address to continue")
      end
    end

    context 'user already exists' do
      let!(:user_params) do
        {
          email: 'user@test.com',
          password: '12345678',
          password_confirmation: '12345678',
          nickname: 'testuser'
        }
      end

      it 'gives error user already exists' do
        post :create, params: { user: user_params }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 422
        expect(json_response["error"]).to eq("User already exists")
      end
    end

    describe '#show' do
      it 'should return user' do
        get :show, params: { id: @user.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
        # expect(json_response["email"]).to eq @user.email
      end
    end

    describe '#update' do
      let!(:user){ create(:user) }
      let!(:skill){ create(:skill) }
      context 'update with interest' do
        it 'should not update if interest is missing' do
          put :update, params: { id: @user.id, user: { nickname: 'test12', interests: [1923] } }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 404
          expect(json_response["message"]).to eq "Couldn't find Interest with 'id'=1923"
        end

        it 'update if interest present in database' do
          put :update, params: { id: @user.id, user: { nickname: 'test12', interests: [3] } }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 200
          expect(json_response["data"]).to eq "User updated"
        end
      end

      context 'update with skill' do
        it 'should update with skill name' do
          put :update, params: { id: @user.id, user: { nickname: 'test12', skills: ['Big Data'] } }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 200
          expect(json_response["data"]).to eq "User updated"
          expect(@user.skills.count).to eq 1
        end
      end

      context 'update with ressource' do
        it 'should update with ressource name' do
          put :update, params: { id: @user.id, user: { nickname: 'test12', ressources: ['3D printers'] } }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 200
          expect(json_response["data"]).to eq "User updated"
          expect(@user.ressources.count).to eq 1
          expect(@user.ressources.first.ressource_name).to match("3D printers")
        end
      end

      context 'update nickname' do
        it 'gives error if nickname already taken' do
          put :update, params: { id: @user.id, user: { nickname: user.nickname } }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 422
          expect(json_response["data"]).to eq "Nickname is already taken"
        end

        it 'should update user' do
          put :update, params: { id: @user.id, user: { nickname: 'test12' } }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 200
          expect(json_response["data"]).to eq "User updated"
          expect(User.find(@user.id).nickname).to eq "test12"
        end
      end
    end

    describe '#destroy' do
      it 'should updated the status of user' do
        delete :destroy, params: { id: @user.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(@user.status).to eq "archived"
      end
    end
  end
end
