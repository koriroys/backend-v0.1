require 'rails_helper'

RSpec.describe Api::PagesController, type: :controller do
  before (:each) do
    @user = User.create!({
      email: 'user@test.com',
      password: 'password123',
      password_confirmation: 'password123'
      })
    @user.confirm
    sign_in @user
    allow_any_instance_of(Api::UsersController).to receive(:current_user).and_return @user
  end
  
  describe '#index' do
    it 'should return list of Pages' do
      get :index
      json_response = JSON.parse(response.body)
      expect(response).to have_http_status :ok
      expect(json_response["content"]).to eq "Welcome to the JOGL API"
    end
  end

  describe '#index' do
    it 'should return Unauthorized error' do
      sign_out @user
      get :index
      json_response = JSON.parse(response.body)
      expect(response).to have_http_status :ok
      expect(json_response["content"]).to eq "Welcome to the JOGL API"
    end
  end
end
