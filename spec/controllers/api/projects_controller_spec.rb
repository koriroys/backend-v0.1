require 'rails_helper'

RSpec.describe Api::ProjectsController, type: :controller do
  before (:each) do
    @user = User.create!({
      email: 'user@test.com',
      password: 'password123',
      password_confirmation: 'password123'
      })
    @user.confirm
    sign_in @user
    allow_any_instance_of(Api::UsersController).to receive(:current_user).and_return @user
    @project = FactoryBot.create(:project, creator_id: @user.id)
  end

  describe '#index' do
    it 'should return list of project' do
      get :index
      project = JSON.parse(response.body)
      expect(response).to have_http_status :ok
      expect(project.count).to eq 1
    end
  end

  describe '#create' do
    context 'duplicate short title error' do
      it 'should not creates a project' do
        post :create, params: { project: @project.attributes }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 422
      end
    end

    context 'creating project with logged in user' do
      it 'should create a project' do
        # @project.short_title = FFaker::Name.first_name
        # post :create, params: { project: @project.attributes.merge(title: @project.short_title) }
        @new_project = FactoryBot.build(:project, creator_id: @user.id)
        post :create, params: { project: @new_project.attributes }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 201
      end
    end

    context 'creating project without logged in user' do
      it 'should not create a project' do
        sign_out @user
        @project.short_title = FFaker::Name.first_name
        post :create, params: { project: @project.attributes }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 401
      end
    end
  end

  describe '#show' do
    it 'should return project' do
      get :show, params: { id: @project.id }
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 200
    end
  end

  describe '#update' do
    context 'Updating project with logged in user and permissons' do
      it 'should update a project' do
        @user.add_role :admin, @project
        put :update, params: { project: { description: "New descriptioniwqe", title: "New title1"}, id: @project.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
      end
    end
    context 'User not authorized for updating project' do
      it 'should throw forbidden error' do
        put :update, params: { project: { description: "New description"}, id: @project.id  }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 403
      end
    end

    context 'updating project without loggin in user' do
      it 'should throw Unauthorized error' do
        sign_out @user
        put :update, params: { project: { description: "New description"}, id: @project.id  }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 401
      end
    end
  end

  describe '#destroy' do
    context 'Deleting project without loggin in user' do
      it 'Should not Delete the project' do
        sign_out @user
        delete :destroy, params: { id: @project.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 401
      end
    end

    context 'Deleting project with loggin in user' do
      it 'Should Delete the project' do
        delete :destroy, params: { id: @project.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
      end
    end
  end

  describe '#customfields' do
    context 'Creating/Updating/Deleting a custom field entry with a non logged in user' do
      it 'Should respond unauthorized (401) and not create the entry' do
        sign_out @user
        post :create_custom_field, params: {  id: @project.id,
                                              fields: {
                                            		description: "Very bad question ?",
                                            		name: "bad",
                                            		field_type: "input",
                                            		optional: true
                                            	        }
                                            }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 401
        expect(@project.customfields.count).to eq 0
      end

      it 'Should respond unauthorized (401) and not update the entry' do
        sign_out @user
        @customfields = @project.customfields.create( description: "Very bad question ?",
                                      name: "bad",
                                      field_type: "input",
                                      optional: true)
        post :update_custom_field, params: {  id: @project.id,
                                              field_id: @customfields.id,
                                              fields: {
                                            		description: "Very bad question ?",
                                            		name: "good",
                                            		field_type: "input",
                                            		optional: true
                                            	        }
                                            }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 401
        expect(@project.customfields.count).to eq 1
        expect(@project.customfields.first[:name]).to match("bad")
      end

      it 'Should respond unauthorized (401) and not delete the entry' do
        sign_out @user
        @customfields = @project.customfields.create( description: "Very bad question ?",
                                      name: "bad",
                                      field_type: "input",
                                      optional: true)
        post :delete_custom_field, params: {  id: @project.id,
                                              field_id: @customfields.id
                                            }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 401
        expect(@project.customfields.count).to eq 1
      end
    end

    context "Getting the list of customfields entry" do
      it "Should anwser with the full list of entries" do
        @customfields = @project.customfields.create( description: "Very bad question ?",
                                      name: "bad",
                                      field_type: "input",
                                      optional: true)
        @customfields = @project.customfields.create( description: "Very bad question ?",
                                      name: "bad",
                                      field_type: "input",
                                      optional: true)

        get :get_custom_fields, params: { id: @project.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(json_response.count).to eq 2
      end
    end

    context 'Creating/Updating/Deleting a custom field entry with a non admin logged in user' do
      it 'Should respond forbidden (403) and not create the entry' do
        post :create_custom_field, params: {  id: @project.id,
                                              fields: {
                                            		description: "Very bad question ?",
                                            		name: "bad",
                                            		field_type: "input",
                                            		optional: true
                                            	        }
                                            }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 403
        expect(@project.customfields.count).to eq 0
      end

      it 'Should respond forbidden (403) and not update the entry' do
        @customfields = @project.customfields.create( description: "Very bad question ?",
                                      name: "bad",
                                      field_type: "input",
                                      optional: true)
        post :update_custom_field, params: {  id: @project.id,
                                              field_id: 1,
                                              fields: {
                                            		description: "Very bad question ?",
                                            		name: "good",
                                            		field_type: "input",
                                            		optional: true
                                            	        }
                                            }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 403
        expect(@project.customfields.count).to eq 1
        expect(@project.customfields.first[:name]).to match("bad")
      end

      it 'Should respond forbidden (403) and not delete the entry' do
        @customfields = @project.customfields.create( description: "Very bad question ?",
                                      name: "bad",
                                      field_type: "input",
                                      optional: true)
        post :delete_custom_field, params: {  id: @project.id,
                                              field_id: @customfields.id
                                            }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 403
        expect(@project.customfields.count).to eq 1
      end
    end

    context 'Creating/updating/deleting a custom field entry with an admin' do
      it 'Should create the custom field entry' do
        @user.add_role :admin, @project
        post :create_custom_field, params: {  id: @project.id,
                                              fields: {
                                            		description: "Very bad question ?",
                                            		name: "bad",
                                            		field_type: "input",
                                            		optional: true
                                            	        }
                                            }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 201
        expect(@project.customfields.count).to eq 1
      end

      it 'Should update the custom field entry' do
        @user.add_role :admin, @project
        @project.customfields.create( description: "Very bad question ?",
                                      name: "bad",
                                      field_type: "input",
                                      optional: true)
        @customfield = @project.customfields.first
        post :update_custom_field, params: {  id: @project.id,
                                              field_id: @customfield.id,
                                              fields: {
                                            		description: "Very good question ?",
                                            		name: "good",
                                            		field_type: "input",
                                            		optional: true
                                            	        }
                                            }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(@project.customfields.count).to eq 1
        expect(@project.customfields.first[:name]).to match("good")
      end

      it 'Should delete the custom field entry' do
        @user.add_role :admin, @project
        @project.customfields.create( description: "Very bad question ?",
                                      name: "bad",
                                      field_type: "input",
                                      optional: true)
        @customfield = @project.customfields.first
        post :delete_custom_field, params: {  id: @project.id,
                                              field_id: @customfield.id
                                            }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(@project.customfields.count).to eq 0
      end
    end
  end

  describe '#customdatas' do

    before (:each) do
      @project.customfields.create( description: "Very bad question ?",
                                    name: "bad",
                                    field_type: "input",
                                    optional: true)
      @customfield = @project.customfields.first
    end

    context 'Creating/Updating/Deleting a custom data entry with a non logged in user' do
      it 'Should respond unauthorized (401) and not create the entry' do
        sign_out @user
        post :create_custom_data, params: { id: @project.id,
                                            field_id: @customfield.id,
                                            data: {
                                                		value: "Very bad answer ?"
                                        	        }}
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 401
        expect(@customfield.customdatas.count).to eq 0
      end

      it 'Should respond unauthorized (401) and not update the entry' do
        sign_out @user
        patch :update_custom_data, params: { id: @project.id,
                                            field_id: @customfield.id,
                                            data: {
                                                		value: "Very bad answer ?"
                                        	        }
                                          }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 401
        expect(@customfield.customdatas.count).to eq 0
      end
    end

    context 'Creating/Updating/Deleting a custom data entry with a non member' do
      it 'Should respond forbidden (403) and not create the entry' do
        post :create_custom_data, params: { id: @project.id,
                                            field_id: @customfield.id,
                                            data: {
                                                		value: "Very bad answer ?"
                                        	        }}
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 403
        expect(@customfield.customdatas.count).to eq 0
      end

      it 'Should respond forbidden (403) and not update the entry' do
        patch :update_custom_data, params: { id: @project.id,
                                            field_id: @customfield.id,
                                            data: {
                                                		value: "Very bad answer ?"
                                        	        }
                                          }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 403
        expect(@customfield.customdatas.count).to eq 0
      end
    end

    context 'Creating/Updating/Deleting a custom data entry with a member' do
      it 'Should create the entry' do
        @user.add_role :member, @project
        post :create_custom_data, params: { id: @project.id,
                                            field_id: @customfield.id,
                                            data: {
                                                		value: "Very bad answer ?"
                                        	        }}
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 201
        expect(@customfield.customdatas.count).to eq 1
      end

      it 'Should update the entry' do
        @user.add_role :member, @project
        @customfield.customdatas.create(user_id: @user.id, value: "something to change")
        patch :update_custom_data, params: { id: @project.id,
                                            field_id: @customfield.id,
                                            data: {
                                                		value: "Very bad good ?"
                                        	        }
                                          }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(@customfield.customdatas.count).to eq 1
        expect(@customfield.customdatas.first[:value]).to match("Very bad good ?")
      end
    end

    context "Getting my customdata entry" do
      it "Should anwser with the full list of entries" do
        @customfield.customdatas.create(user_id: @user.id, value: "something to change")
        @user.add_role :member, @project
        get :get_my_custom_datas, params: { id: @project.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(json_response.count).to eq 1
      end

      it "Should not work if you are not logged in" do
        @customfield.customdatas.create(user_id: @user.id, value: "something to change")
        sign_out @user

        get :get_my_custom_datas, params: { id: @project.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 401
      end

      it "Should not work if you are not a member" do
        @customfield.customdatas.create(user_id: @user.id, value: "something to change")

        get :get_my_custom_datas, params: { id: @project.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 403
      end
    end

    context "Getting the project customdata entry" do
      it "Should anwser with the full list of entries" do
        @customfield.customdatas.create(user_id: @user.id, value: "something to change")
        @user.add_role :admin, @project

        get :get_custom_data, params: { id: @project.id, field_id: @customfield.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(json_response.count).to eq 1
      end

      it "Should not work if you are not admin" do
        @customfield.customdatas.create(user_id: @user.id, value: "something to change")

        get :get_custom_data, params: { id: @project.id, field_id: @customfield.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 403
      end


      it "Should not work if you are not logged in" do
        @customfield.customdatas.create(user_id: @user.id, value: "something to change")
        sign_out @user

        get :get_custom_data, params: { id: @project.id, field_id: @customfield.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 401
      end
    end

  end

end
