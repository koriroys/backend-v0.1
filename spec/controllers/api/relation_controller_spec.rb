require 'rails_helper'

RSpec.describe Api::ProjectsController, type: :controller do
  before (:each) do
    @user = User.create!({
      email: 'user@test.com',
      password: 'password123',
      password_confirmation: 'password123'
      })
    @user.confirm
    sign_in @user
    allow_any_instance_of(Api::UsersController).to receive(:current_user).and_return @user
    @project = FactoryBot.create(:project, creator_id: @user.id)
    # @user2 = FactoryBot.create(:confirmed_user)
    # @user3 = FactoryBot.create(:confirmed_user)
    # @project.users << @user2
    # @project.users << @user3
    # @user.add_role :member, @project
    # @user2.add_role :member, @project
    # @user3.add_role :member, @project
  end

  describe '#follow' do
    it 'should follow the project' do
      put :follow, params: { id: @project.id }
      json_response = JSON.parse(response.body)
      expect(response).to have_http_status :ok
      expect(@project.followers.count).to eq 1
    end

    it 'should require auth' do
      sign_out @user
      put :follow, params: { id: @project.id }
      json_response = JSON.parse(response.body)
      expect(response).to have_http_status :unauthorized
      expect(@project.followers.count).to eq 0
    end
  end

  describe '#unfollow' do
    it 'should unfollow the project' do
      put :follow, params: { id: @project.id }
      expect(@project.followers.count).to eq 1
      delete :follow, params: { id: @project.id }
      json_response = JSON.parse(response.body)
      expect(response).to have_http_status :ok
      expect(@project.followers.count).to eq 0
    end

    it 'should require auth' do
      sign_out @user
      delete :follow, params: { id: @project.id }
      json_response = JSON.parse(response.body)
      expect(response).to have_http_status :unauthorized
      expect(@project.followers.count).to eq 0
    end
  end

  describe '#is_follow' do
    before (:each) do
      put :follow, params: { id: @project.id }
    end

    it 'should anwser true' do
      get :follow, params: { id: @project.id }
      json_response = JSON.parse(response.body)
      expect(response).to have_http_status :ok
      expect(json_response["follows"]).to eq true
    end

    it 'should anwser false' do
      @relation = Relation.find_by(user_id: @user.id, resource_type: @project.class.name, resource_id: @project.id)
      @relation.follows = false
      @relation.save
      get :follow, params: { id: @project.id }
      json_response = JSON.parse(response.body)
      expect(response).to have_http_status :ok
      expect(json_response["follows"]).to eq false
    end

    it 'should anwser no_found' do
      @relation = Relation.find_by(user_id: @user.id, resource_type: @project.class.name, resource_id: @project.id)
      @relation.delete
      get :follow, params: { id: @project.id }
      json_response = JSON.parse(response.body)
      expect(response).to have_http_status :not_found
    end

#TODO WHY IS THIS NOT WORKING ???

    # it 'should require auth' do
    #   sign_out @user
    #   get :follow, params: { id: @project.id }
    #   json_response = JSON.parse(response.body)
    #   expect(response).to have_http_status :unauthorized
    # end
  end

  describe '#followers' do
    before (:each) do
      put :follow, params: { id: @project.id }
    end

    it 'should get the followers of the project' do
      get :followers, params: { id: @project.id }
      json_response = JSON.parse(response.body)
      expect(response).to have_http_status :ok
      expect(json_response["followers"].count).to eq 1
    end
  end


    describe '#clap' do
      it 'should clap the project' do
        put :clap, params: { id: @project.id }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(@project.clappers.count).to eq 1
      end

      it 'should require auth' do
        sign_out @user
        put :clap, params: { id: @project.id }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :unauthorized
        expect(@project.clappers.count).to eq 0
      end
    end

    describe '#unclap' do
      it 'should unclap the project' do
        put :clap, params: { id: @project.id }
        expect(@project.clappers.count).to eq 1
        delete :clap, params: { id: @project.id }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(@project.clappers.count).to eq 0
      end

      it 'should require auth' do
        sign_out @user
        delete :clap, params: { id: @project.id }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :unauthorized
        expect(@project.clappers.count).to eq 0
      end
    end

    describe '#is_clap' do
      before (:each) do
        put :clap, params: { id: @project.id }
      end

      it 'should anwser true' do
        get :clap, params: { id: @project.id }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(json_response["has_clapped"]).to eq true
      end

      it 'should anwser false' do
        @relation = Relation.find_by(user_id: @user.id, resource_type: @project.class.name, resource_id: @project.id)
        @relation.has_clapped = false
        @relation.save
        get :clap, params: { id: @project.id }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(json_response["has_clapped"]).to eq false
      end

      it 'should anwser no_found' do
        @relation = Relation.find_by(user_id: @user.id, resource_type: @project.class.name, resource_id: @project.id)
        @relation.delete
        get :clap, params: { id: @project.id }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :not_found
      end

  #TODO WHY IS THIS NOT WORKING ???

      # it 'should require auth' do
      #   sign_out @user
      #   get :clap, params: { id: @project.id }
      #   json_response = JSON.parse(response.body)
      #   expect(response).to have_http_status :unauthorized
      # end
    end

    describe '#clappers' do
      before (:each) do
        put :clap, params: { id: @project.id }
      end

      it 'should get the clappers of the project' do
        get :clappers, params: { id: @project.id }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(json_response["clappers"].count).to eq 1
      end
    end

end


#TODO ADD SPECS FOR MEMBERS HANDLING HERE FOR ALL OBJECTS
