require 'rails_helper' 
# require 'sidekiq/testing'

RSpec.describe NotificationEmailWorker, type: :worker do
	describe 'testing worker' do

    # it "should respond to #perform" do
    #   user = FactoryBot.create(:user)
    #   message = "message"
    #   url = "logl/backend/spec"
    #   title = "title"
    #   attachment_path = "participation_rules.pdf"
    #   expect(NotificationMailer.notify(user, message, url, title, attachment_path)).to respond_to(:perform)
    # end

    # describe "perform" do
    #   before do
    #     Sidekiq::Worker.clear_all
    #   end
    # end

  	it "should sends to right email" do
      user = FactoryBot.create(:user)
      message = "message"
      url = Rails.root
      title = "title"
      mail = NotificationMailer.notify(user, message, url, title, nil)
      expect(mail.to).to eq ([user.email])
    end

  	it "should renders the subject" do
      user = FactoryBot.create(:user)
      message = "message"
      url = Rails.root
      title = "title"
      mail = NotificationMailer.notify(user, message, url, title, nil)
      expect(mail.subject).to eq "#{title}"
    end
  end
end