require 'rails_helper' 
# require 'sidekiq/testing'

RSpec.describe InviteUserEmailWorker, type: :worker do
	describe 'testing worker' do

    # it "should respond to #perform" do
    #   owner = FactoryBot.create(:user)
    #   object = FactoryBot.create(:project, creator_id: owner.id)
    #   joiner = FactoryBot.create(:user)
    #   expect{InviteUserEmailWorker.perform_async(owner.id, object.class.to_s, object.id, joiner.id)}.to change{InviteUserEmailWorker.jobs.count}.by(1)
    # end

    # describe "perform" do
    #   before do
    #     FactoryBot.factories.clear
    #     Sidekiq::Worker.clear_all
    #   end
    # end

  	it "should sends to right email" do
      owner = FactoryBot.create(:user)
      object = FactoryBot.create(:project, creator_id: owner.id)
      joiner = FactoryBot.create(:user)
      mail = InviteUserMailer.invite_user(owner, object, joiner)
      expect(mail.to).to eq ([joiner.email])
    end

  	it "should renders the subject" do
      owner = FactoryBot.create(:user)
      object = FactoryBot.create(:project, creator_id: owner.id)
      joiner = FactoryBot.create(:user)
      mail = InviteUserMailer.invite_user(owner, object, joiner)
      expect(mail.subject).to eq "You have been invited to join a project"
    end
  end
end