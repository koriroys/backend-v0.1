require 'rails_helper'

RSpec.describe Mention, type: :model do
  describe 'association' do
    it { should belong_to(:post) }
  end

  describe 'validation' do
    let!(:post) { create(:post) }
    let!(:mention) { create(:mention, post: post) }
    it "should have valid factory" do
      expect(mention).to be_valid
    end

    it "should have unique post" do
      mention2 = FactoryBot.build(:mention, :post => mention.post)
      expect(mention2).not_to be_valid
    end
  end
end
