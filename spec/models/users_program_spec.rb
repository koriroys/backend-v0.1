require 'rails_helper'

RSpec.describe UsersProgram, type: :model do
  describe 'associations' do
    it { should belong_to(:user) }
    it { should belong_to(:program) }
  end
end
