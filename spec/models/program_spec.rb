require 'rails_helper'

RSpec.describe Program, type: :model do
  
  describe 'associations' do
    it { should have_many(:challenges) }
    it { should have_many(:users_programs) }
    it { should have_one(:feed) }
  end

  describe 'validation' do
    let!(:program) { create(:program) }
    it "should have valid factory" do
      expect(FactoryBot.build(:program)).to be_valid
    end

    # it "should have unique short title" do
    #   program2 = FactoryBot.build(:program, :short_title => program.short_title)
    #   expect(FactoryBot.build(:program)).to be_valid
    #   expect(program2.errors[:short_title]).to include("has already been taken")
    # end
  end

  describe "#create_feed" do
    it "triggers create feed on create" do
      program = FactoryBot.build(:program)
      expect(program).to receive(:create_feed)
      program.save
    end
  end
end
