require 'rails_helper'

RSpec.describe Project, type: :model do
  
  describe 'associations' do
    it { should have_many(:challenges) }
    it { should have_many(:users_projects) }
    it { should have_one(:feed) }
    it { should have_and_belong_to_many(:skills) }
    it { should have_and_belong_to_many(:interests) }
  end

  # describe "#create_feed" do
  #   it "triggers create feed on create" do
  #     project = FactoryBot.build(:project)
  #     expect(project).to receive(:create_feed)
  #     project.save
  #   end
  # end

  describe 'validation' do
    let!(:project) { create(:project, creator_id: FactoryBot.create(:user).id) }
    it "should have valid factory" do
      expect(project).to be_valid
    end

    # it "should have unique short title" do
    #   project2 = FactoryBot.build(:project, :short_title => project.short_title)
    #   expect(project2).not_to be_valid
    #   expect(project2.errors[:short_title]).to include("has already been taken")
    # end
  end
end
