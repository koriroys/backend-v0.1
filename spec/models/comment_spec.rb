require 'rails_helper'

RSpec.describe Comment, type: :model do
  describe 'associations' do
    it { should belong_to(:post) }
    it { should belong_to(:user) }
  end

  describe 'validation' do
    it "should have valid factory" do
      expect(FactoryBot.build(:comment)).to be_valid
    end

    it "should require content" do
      comment = FactoryBot.build(:comment, :content => "")
      expect(comment).not_to be_valid
    end
  end
end
