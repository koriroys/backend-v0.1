require 'rails_helper'

RSpec.describe Challenge, type: :model do
  describe 'associations' do
    it { should have_many(:challenges_users) }
    it { should have_and_belong_to_many(:skills) }
    it { should have_and_belong_to_many(:interests) }
    it { should have_one(:feed) } 
  end

  describe "#create_feed" do
    it "triggers create feed on create" do
      challenge = FactoryBot.build(:challenge)
      expect(challenge).to receive(:create_feed)
      challenge.save
    end
  end
end
