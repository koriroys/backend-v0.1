require 'rails_helper'

RSpec.describe Community, type: :model do
  describe 'associations' do
    it { should have_and_belong_to_many(:community_tags) }
    it { should have_and_belong_to_many(:skills) }
    it { should have_and_belong_to_many(:interests) }
    it { should have_one(:feed) }
    it { should have_many(:users_communities) }
    it { should have_many(:challenges_communities) }
  end

  describe "#create_feed" do
    it "triggers create feed on create" do
      community = FactoryBot.build(:community)
      expect(community).to receive(:create_feed)
      community.save
    end
  end

  describe 'validation' do
    let!(:community) { create(:community) }
    it "should have valid factory" do
      expect(community).to be_valid
    end

    it "should have unique short title" do
      community2 = FactoryBot.build(:community, :short_title => community.short_title)
      expect(community2).not_to be_valid
      expect(community2.errors[:short_title]).to include("has already been taken")
    end
  end
end