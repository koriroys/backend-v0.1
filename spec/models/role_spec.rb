require 'rails_helper'

RSpec.describe Role, type: :model do
  let!(:user) { create(:user) }

  it "should not approve incorrect roles" do
    user.add_role :admin
    expect(user.has_role?(:moderator)).to be false
  end

  it "should approve correct roles" do
    user.add_role :admin
    expect(user.has_role?(:admin)).to be true
  end
end
