require 'rails_helper'

RSpec.describe Need, type: :model do
  
  describe 'associations' do
    it { should belong_to(:user) }
    it { should belong_to(:project) }
    it { should have_one(:feed) }
  end

  describe 'validation' do
    it "should have valid factory" do
      expect(FactoryBot.build(:need)).to be_valid
    end

    it "should require title" do
      need = FactoryBot.build(:need, :title => "")
      expect(need).not_to be_valid
    end

    it "should require content" do
      need = FactoryBot.build(:need, :content => "")
      expect(need).not_to be_valid
    end
  end

  describe "#create_feed" do
    it "triggers create feed on create" do
      need = FactoryBot.build(:need)
      expect(need).to receive(:create_feed)
      need.save
    end
  end
end
