require 'rails_helper'

RSpec.describe NeedsSkill, type: :model do
  describe 'associations' do
    it { should belong_to(:need) }
    it { should belong_to(:skill) }
  end
end
