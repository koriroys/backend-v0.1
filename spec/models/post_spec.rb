require 'rails_helper'

RSpec.describe Post, type: :model do
  
  describe 'associations' do
    it { should belong_to(:user) }
    it { should have_and_belong_to_many(:feeds) }
    it { should have_many(:comments) }
    it { should have_many(:mentions) }
  end

  describe 'validation' do
    it "should have valid factory" do
      expect(FactoryBot.build(:post)).to be_valid
    end

    it "should require content" do
      post = FactoryBot.build(:post, :content => "")
      expect(post).not_to be_valid
    end
  end
end
