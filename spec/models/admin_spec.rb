require 'rails_helper'

RSpec.describe Admin, type: :model do
  it 'is database authenticable' do
    admin = Admin.create(
      email: 'testadmin@jogl.com', 
      password: 'password123',
      password_confirmation: 'password123'
    )
    expect(admin.valid_password?('password123')).to be_truthy
  end
end
