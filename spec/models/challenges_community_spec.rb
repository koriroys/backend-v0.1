require 'rails_helper'

RSpec.describe ChallengesCommunity, type: :model do
  describe 'associations' do
    it { should belong_to(:challenge) }
    it { should belong_to(:community) }
  end
end
