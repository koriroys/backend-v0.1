class InterestsCommunity < ActiveRecord::Migration[5.2]
  def change
    create_table :interests_communities, id: false do |t|
      t.integer :communities_id
      t.integer :interest_id
    end
  end
end
