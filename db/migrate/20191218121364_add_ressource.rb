class AddRessource < ActiveRecord::Migration[5.2]
  def change
    create_table :ressources, force: :cascade do |t|
      t.references :ressourceable, polymorphic: true
      t.string :ressource_name
    end
  end
end
