class CreateExternalHook < ActiveRecord::Migration[5.2]
  def change
    create_table :externalhooks do |t|
      t.references :hookable, polymorphic: true
      t.string "hook_type"
      t.boolean "trigger_need"
      t.boolean "trigger_post"
      t.boolean "trigger_member"
      t.boolean "trigger_project"
    end

    create_table :slackhooks do |t|
      t.belongs_to :externalhook, index: true
      t.string "hook_url"
      t.string "channel"
      t.string "username"
    end

  end
end
