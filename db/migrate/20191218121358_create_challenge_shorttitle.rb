class CreateChallengeShorttitle < ActiveRecord::Migration[5.2]
  def change
    add_column :challenges, :short_title, :string
  end
end
