class AddLockableToDevise < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :locked_at, :datetime
    add_index :users, :confirm_token, unique: true
  end
end
