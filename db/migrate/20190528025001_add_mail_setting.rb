class AddMailSetting < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :mail_newsletter, :bool
    add_column :users, :mail_weekly, :bool
  end
end
