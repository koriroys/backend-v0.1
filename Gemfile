source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.3'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.1'
# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'
# Use Puma as the app server
gem 'puma'
# Use Redis adapter to run Action Cable in production
gem 'redis'
# Use ActiveModel has_secure_password
gem 'bcrypt'

gem 'devise_token_auth'

gem 'devise'

gem 'devise-async'

gem 'cancancan'

gem 'rolify'

gem 'multipart-post'

gem 'dotenv-rails', groups: [:development, :test]

gem 'active_model_serializers'
# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rack-cors', require: 'rack/cors'

# Slack

gem 'slack-notifier'

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

# Algolia search engine
gem "algoliasearch-rails"

gem "seedbank"

gem "sidekiq"
gem "sidekiq-cron"

gem "mini_magick"

gem "aws-sdk-s3", require: false

gem 'gibbon'

gem "et-orbi"

gem 'pagy'

# Adding Swagger and rspec
group :development, :test do
  gem 'factory_bot_rails'
  gem 'rspec-rails', '~> 3.5'
  gem "rspec-rails-swagger"
  gem 'database_cleaner'
  gem "ffaker"
  gem "rails-controller-testing"
  gem 'shoulda'
  gem 'simplecov'
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end

# Adding GeoCoding capacity
gem "geocoder"

# Adding ELK logstash system
gem 'logstasher'
gem 'logstash-logger'
