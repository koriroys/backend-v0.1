#!/bin/sh

# Adapted from https://toedter.com/2018/06/02/heroku-docker-deployment-update/

imageId=$(docker inspect registry.heroku.com/$1/web --format={{.Id}})
payload='{"updates":[{"type":"web","docker_image":"'"$imageId"'"}]}'

curl -n -X PATCH https://api.heroku.com/apps/$1/formation \
-d "$payload" \
-H "Content-Type: application/json" \
-H "Accept: application/vnd.heroku+json; version=3.docker-releases" \
-H "Authorization: Bearer $HEROKU_TOKEN"

curl -n -X POST https://api.heroku.com/apps/$1/dynos \
	-H "Content-Type: application/json" \
	-H "Accept: application/vnd.heroku+json; version=3" \
	-H "Authorization: Bearer ${HEROKU_TOKEN}" \
	-d '{ "command": "bundle exec rails db:migrate db:seed db:seed:interests", "type": "run"}' 

imageId=$(docker inspect registry.heroku.com/$1/sidekiq --format={{.Id}})
payload='{"updates":[{"type":"sidekiq","docker_image":"'"$imageId"'"}]}'

curl -n -X PATCH https://api.heroku.com/apps/$1/formation \
-d "$payload" \
-H "Content-Type: application/json" \
-H "Accept: application/vnd.heroku+json; version=3.docker-releases" \
-H "Authorization: Bearer $HEROKU_TOKEN"
